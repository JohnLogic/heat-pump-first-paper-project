# Based on https://gitlab.com/JohnLogic/ct-reconstruction/-/blob/master/utils.py
# from my MSc thesis project

import math
import os
from datetime import datetime
from pathlib import Path
from typing import Any, Optional

import dateutil
import git
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class FileUtils:

    DEFAULT_TEMP_FOLDER = "./tmp"

    @classmethod
    def apth(cls, path_string) -> str:
        path_obj = Path(path_string)
        absolute_path = str(path_obj.resolve())
        return cls.pth(absolute_path)

    @classmethod
    def pth(cls, path_string) -> str:
        return os.path.join(*path_string.split('/'))

    @classmethod
    def create_dir(cls, path_string : str, fail_if_exists : bool = False) -> str:
        path_obj = Path(path_string)
        path_obj.mkdir(parents=True, exist_ok=not fail_if_exists)
        return str(path_obj.resolve())

    @classmethod
    def file_name(cls, path_string : str, with_extension : bool = False) -> str:
        path_obj = Path(path_string)
        return path_obj.stem if not with_extension else path_obj.name

    @classmethod
    def home_dir(cls) -> str:
        return str(Path.home())

class DateUtils:

    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H-%M-%S"
    DATETIME_FORMAT = "{}_{}".format(DATE_FORMAT, TIME_FORMAT)

    @classmethod
    def now(cls):
        now = datetime.now()
        return now

    @classmethod
    def formatted_datetime_now(cls):
        return datetime.now().strftime(cls.DATETIME_FORMAT)

class ListUtils:

    @classmethod
    def natural_sort(cls, list_to_sort: [str]) -> [str]:
        """ Sort the given list of strings in the way that humans expect.
        """
        import copy, re
        copied_list = copy.deepcopy(list_to_sort)
        convert = lambda text: int(text) if text.isdigit() else text
        alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
        copied_list.sort(key=alphanum_key)
        return copied_list

class FunkyUtils:

    @classmethod
    def len_iterable(cls, iterable):
        # Based on: https://stackoverflow.com/questions/31011631/python-2-3-object-of-type-zip-has-no-len
        return sum(1 for _ in iterable)

class VisualizationUtils:
    @classmethod
    def visualize_temperatures_and_power(cls, df: pd.DataFrame, power_col: str,
                                         outdoor_temp_col: str, indoor_temp_col: str,
                                         legend_loc: str = 'best',
                                         title_label: str = 'Combined temperatures and power'):

        power_df = df[[power_col]]
        indoor_temp_df = df[[indoor_temp_col]]
        outdoor_temp_df = df[[outdoor_temp_col]]

        ax1 = outdoor_temp_df.plot(color="dodgerblue", figsize=(10, 5), legend=True)
        ax1.set_ylabel('Temperature (Celsius)')
        ax2 = indoor_temp_df.plot(color="red",
                                  ax=ax1,
                                  legend=False,
                                  title=title_label,
                                  x_compat=True)
        ax3 = power_df.plot(secondary_y=True, color="gray", ax=ax2, legend=True, rot=90, x_compat=True,
                            linewidth=3, alpha=0.5)
        ax3.set_ylabel('Power (Watts)')

        # MANUAL legend control
        # Link: https://stackoverflow.com/a/21989204
        lines = ax2.get_lines() + ax2.right_ax.get_lines()

        def replace_labels(orig_label):
            if orig_label == outdoor_temp_col:
                return "Outdoor temperature"
            elif orig_label == indoor_temp_col:
                return "Indoor temperature"
            elif orig_label == power_col:
                return "Power"

        custom_labels = list(map(replace_labels, [l.get_label() for l in lines]))
        ax2.legend(lines, custom_labels, loc=legend_loc)

        return ax3


    @classmethod
    def visualizeSinglePrediction(cls,
                                  idx: int,
                                  X_batch,
                                  y_batch,
                                  y_batch_hat,
                                  test_dm,
                                  loss_val,
                                  img_output_path_prefix,
                                  show_last_x: int = 4,
                                  ):
        plt.close()


        x_len = X_batch.shape[1]
        y_len = y_batch.shape[1]

        x_timesteps = list(range(1, show_last_x + 1))
        y_timesteps = list(range(show_last_x + 1, show_last_x + y_len + 1))

        X_r = X_batch.reshape(-1, 3)
        y_r = y_batch.reshape(4)
        y_hat_r = y_batch_hat.reshape(4)

        last_x = (test_dm.scaler.inverse_transform(X_r[-show_last_x:]))

        # Plot config
        plt.title(f"i={idx} | " + "RMSE={:.3f}".format(loss_val))

        x_idx, xs_list, xos_list, u_list = 0, [], [], []
        for x, xo, u in (last_x):
            xs_list.append(x)
            xos_list.append(xo)
            u_list.append(u)
            x_idx += 1

        plt.xlabel("Time (15 min increments)")
        plt.ylabel("Temperature")
        plt.plot(list(x_timesteps) + list(y_timesteps), list(xs_list) + list(y_r), label='real ind. temperature',
                 linewidth=2, color='red')
        plt.plot(y_timesteps, y_hat_r.detach().numpy(), linestyle='dashed', label='predicted ind. temperature',
                 color='black', linewidth=2)
        plt.legend()
        plt.savefig(img_output_path_prefix + '.png')

    @classmethod
    def visualizeSinglePredictionBetter(cls,
                                  X_batch,
                                  y_batch,
                                  y_batch_hat,
                                  n_features: int,
                                  test_dm,
                                  img_output_path_prefix: str,
                                  plot_title: str,
                                  show_last_x: int = 4,
                                  show_control_signal_from_n_last_columns: Optional[int] = None,
                                  ):
        # Based on: https://matplotlib.org/3.1.1/gallery/ticks_and_spines/multiple_yaxis_with_spines.html
        plt.close()

        x_len = X_batch.shape[1]
        y_len = y_batch.shape[1]

        x_timesteps = list(range(1, show_last_x + 1))
        y_timesteps = list(range(show_last_x + 1, show_last_x + y_len + 1))

        #X_r 16,13
        #y_r 4
        X_r = X_batch.reshape(-1, n_features)
        y_r = y_batch.reshape(4)
        y_hat_r = y_batch_hat.reshape(4)

        last_x = (X_r.numpy()[-show_last_x:])

        x_idx, xs_list, xos_list, u_list = 0, [], [], []
        control_signal = None
        first_features = None

        if (show_control_signal_from_n_last_columns is not None):
            control_signal = []
        for features in last_x:
            if (control_signal is not None and len(control_signal) == 0):
                control_signal = features[-4:]
            if (first_features is None):
                first_features = features
            x, xo, u = features[0:3]
            xs_list.append(x)
            xos_list.append(xo)
            u_list.append(u)
            x_idx += 1

        # Plot config
        # plt.title(f"i={idx} | " + "RMSE={:.3f}".format(loss_val))
        #
        # plt.xlabel("Time (15 min increments)")
        # plt.ylabel("Temperature")
        # plt.plot(list(x_timesteps) + list(y_timesteps), list(xs_list) + list(y_r), label='real ind. temperature',
        #          linewidth=2, color='red')
        # plt.plot(y_timesteps, y_hat_r.detach().numpy(), linestyle='dashed', label='predicted ind. temperature',
        #          color='black', linewidth=2)
        # plt.legend()
        # plt.savefig(img_output_path_prefix + '.png')

        # ---------------------------------------

        plt.close()

        def make_patch_spines_invisible(ax):
            ax.set_frame_on(True)
            ax.patch.set_visible(False)
            for sp in ax.spines.values():
                sp.set_visible(False)

        fig, host = plt.subplots()
        fig.set_size_inches(8, 6) # <<< NEW
        fig.subplots_adjust(right=0.75)
        fig.suptitle(plot_title)

        par1 = host.twinx()
        par2 = host.twinx()

        # Offset the right spine of par2.  The ticks and label have already been
        # placed on the right by twinx above.
        par2.spines["right"].set_position(("axes", 1.2))
        # Having been created by twinx, par2 has its frame off, so the line of its
        # detached spine is invisible.  First, activate the frame but make the patch
        # and spines invisible.
        make_patch_spines_invisible(par2)
        # Second, show the right spine.
        par2.spines["right"].set_visible(True)

        p1, = host.plot(list(x_timesteps), list(xs_list), color='red', label="Indoor temperature", linewidth=2)
        p1, = host.plot(y_timesteps, y_hat_r.detach().numpy(), color='black', linestyle='dashed',
                        label="Indoor temperature", linewidth=2)
        p1, = host.plot(list(y_timesteps), list(y_r), color='red', label="Indoor temperature", linewidth=2)
        p2, = par1.plot(list(x_timesteps), list(xos_list), color="blue", label="Outdoor temperature", linewidth=2)
        p3, = par2.plot(list(x_timesteps), list(u_list), color="gray", label="Power", linewidth=3,
                        alpha=0.5,)
        par2.fill_between(list(x_timesteps), list(u_list), alpha=0.40, color="lightgray")
        if (control_signal is not None):
            p3, = par2.plot(list(y_timesteps), list(control_signal), color="gray", label="Power", linewidth=2,
                            linestyle='dashed', alpha=0.75,)
            par2.fill_between(list(y_timesteps), list(control_signal), alpha=0.40, color="lightgray")


        # ax3 = power_df.plot(secondary_y=True, color="gray", ax=ax2, legend=True, rot=90, x_compat=True,
        #                     linewidth=3, alpha=0.5)
        # rot = 90, x_compat = True

        padding_ind = 0.5
        padding_outdoor = 2.0
        ind_list=list(xs_list)+list(y_r)+list(y_hat_r.detach().numpy())
        out_list=list(xos_list)

        # NOTE: these are to manually adjust the values
        # host.set_xlim(0, 2)
        host.set_xticks(list(x_timesteps)+list(y_timesteps))
        host.set_ylim(min(ind_list) - 2*padding_ind, max(ind_list) + 0.5*padding_ind) # For indoor temperature (incl. forecast)
        par1.set_ylim(min(out_list) - padding_outdoor*0.5, max(out_list) + padding_outdoor*1.5) # For outdoor temperature
        par2.set_ylim(-10, max(1000, max(list(u_list))) ) # For power

        year=round(first_features[5])
        month=round(first_features[4])
        day=round(first_features[3])
        hour=round(first_features[6])
        minute=round(first_features[7])

        host.set_xlabel(f"Time step (15 min) - start {year}-{month:02}-{day} {hour:02}:{minute:02}")
        host.set_ylabel("Indoor temperature")
        par1.set_ylabel("Outdoor temperature")
        par2.set_ylabel("Power")

        host.yaxis.label.set_color(p1.get_color())
        par1.yaxis.label.set_color(p2.get_color())
        par2.yaxis.label.set_color(p3.get_color())

        tkw = dict(size=4, width=1.5)
        host.tick_params(axis='y', colors=p1.get_color(), **tkw)
        par1.tick_params(axis='y', colors=p2.get_color(), **tkw)
        par2.tick_params(axis='y', colors=p3.get_color(), **tkw)
        host.tick_params(axis='x', **tkw)

        lines = [p1, p2, p3]

        # host.title(f"i={idx} | " + "RMSE={:.3f}".format(float(loss_val)))
        # host.legend(lines, [l.get_label() for l in lines])
        plt.savefig(img_output_path_prefix + '.png')

class DataPreparationUtils:

    DEFAULT_OUTLIER_TEMP_THRESHOLD : float = 15.

    @classmethod
    # Warning: outlier_fix modifies the 'df' instead of creating a copy
    def outlier_fix(cls, df: pd.DataFrame,
                    room_temp_col: str,
                    ambient_temp_col: str,
                    outlier_temp_threshold: float = DEFAULT_OUTLIER_TEMP_THRESHOLD):
        from typing import Optional

        print("Warning: outlier_fix modifies the 'df' instead of creating a copy")

        room_temp_col = room_temp_col
        ambient_temp_col = ambient_temp_col

        # We start from 1 already, might be a problem ???
        i = 1
        length_df = len(df)
        prev_proper_room_temp: Optional[float] = None
        prev_proper_ambient_temp: Optional[float] = None

        while i < length_df:
            prev_record = df.iloc[i - 1]
            current_record = df.iloc[i]

            # Dealing with ambient temperature
            if (prev_proper_room_temp is not None):
                if abs(prev_proper_room_temp - current_record[room_temp_col]) <= outlier_temp_threshold:
                    prev_proper_room_temp = None
                else:
                    df.iloc[i][room_temp_col] = np.nan
            elif abs(prev_record[room_temp_col] - current_record[room_temp_col]) > outlier_temp_threshold:
                # print(f"PROBLEM room_temp at i={i}, prev={prev_record}, curr={current_record}")
                prev_proper_room_temp = prev_record[room_temp_col]
                df.iloc[i][room_temp_col] = np.nan

            # Dealing with ambient temperature
            if (prev_proper_ambient_temp is not None):
                if abs(prev_proper_ambient_temp - current_record[ambient_temp_col]) <= outlier_temp_threshold:
                    prev_proper_ambient_temp = None
                else:
                    df.iloc[i][ambient_temp_col] = np.nan
            elif abs(prev_record[ambient_temp_col] - current_record[ambient_temp_col]) > outlier_temp_threshold:
                # print(f"PROBLEM ambient_temp at i={i}, prev={prev_record}, curr={current_record}")
                prev_proper_ambient_temp = prev_record[ambient_temp_col]
                df.iloc[i][ambient_temp_col] = np.nan

            i += 1
            # break
        pass

    # Splits data into intervals of form:
    # [ POWER_CONSUMPTION [1..m] .. NO_POWER_COMSUMPTION [m+1, n] ]
    # include_power_use - True (Include power consumption in intervals)
    @classmethod
    def split_data_into_intervals(cls,
                                  df: pd.DataFrame,
                                  minimum_interval_length: int,
                                  maximum_interval_length: int,
                                  max_idle_power: float = 100.,
                                  power_col_name: str = 'SumPower',
                                  include_power_use=True,
                                  minimum_power_off_interval_length: int = 0,
                                  ):

        # Constructs the intervals
        total_split_list, single_split_list = [], []
        power_cycle = False
        checkpoint_length = math.floor(len(df) / 10)
        total_length = len(df)
        idx = 0
        print("Processed / Total rows")
        for item in df.iterrows():
            if (idx % checkpoint_length == 0):
                print(f"{str(idx)}/{str(total_length)}")
            idx += 1

            record = item[1]
            record_power = record[power_col_name]

            # If it's the end of a single interval (no power, power case)
            if (record_power > max_idle_power and not (power_cycle)):
                power_cycle = True
                total_split_list.append(pd.DataFrame(single_split_list))
                if (include_power_use):
                    single_split_list = [record]
                else:
                    single_split_list = []
                continue
            # If it's continuing power consumption (power, power case)
            elif (record_power > max_idle_power and power_cycle):
                if not include_power_use:  # Skip if we don't want to include
                    continue
            # If it's the end of power consumption of interval (power, no power case)
            elif (record_power <= max_idle_power and power_cycle):
                power_cycle = False
            single_split_list.append(record)

        print(f"{total_length}/{total_length}")

        # Filters the intervals
        usable_split_list = []
        for interval in total_split_list:
            usable_interval = None
            interval_length = len(interval)
            if (interval.isnull().values.any()):
                continue  # we drop 'dirty' intervals containing nulls
            # if (len(interval[interval[power_col_name] <= max_idle_power]) < minimum_power_off_interval_length):
            #     continue
            if (interval_length < minimum_interval_length):
                continue
            elif (interval_length > maximum_interval_length):
                # Trim to fit the size
                usable_interval = interval.head(maximum_interval_length)
            else:
                usable_interval = interval
            usable_split_list.append(usable_interval)

        return usable_split_list

    @classmethod
    def fail_if_nan(cls, array: np.array, array_name: Optional[str] = None):
        if (np.isnan(array).any()):
            raise ValueError(f"Given {'' if array_name is None else array_name} array has NaN values")

    @classmethod
    def every_n_train_test_split(cls, lst: [Any], n=5) -> ([Any], [Any]):
        train_lst, test_lst = ([], [])
        for idx, item in enumerate(lst):
            if (idx % n == 0):
                test_lst.append(item)
            else:
                train_lst.append(item)

        assert (len(lst) == len(train_lst) + len(test_lst))
        return train_lst, test_lst

class DataAnalysisUtils:

    @classmethod
    def analyseIntervals(cls, test_interval_list: [pd.DataFrame],
                         power_col_name: str = 'SumPower',
                         idle_power_threshold: int = 0,
                         parse_dates: bool = True,
                         dataset_name_label = "",
                         # TODO: compute granularity on it's own
                         dataset_granularity_label = "",
                         minimum_usable_length: int = None):
        # Interval analysis (minimum, maximum, average lengths - total and group-wise

        analysis_obj_list = []

        # Creates {on_len, off_len, timestamp} list
        for interval_df in test_interval_list:
            only_power_on_interval_df = interval_df[interval_df[power_col_name] > idle_power_threshold]
            only_power_off_interval_df = interval_df[interval_df[power_col_name] <= idle_power_threshold]
            analysis_obj = {
                'on_len': len(only_power_on_interval_df),
                'off_len': len(only_power_off_interval_df),
                'timestamp': dateutil.parser.parse(interval_df.index.min()) \
                    if parse_dates else interval_df.index.min(),
                'month': dateutil.parser.parse(interval_df.index.min()).month \
                    if parse_dates else interval_df.index.min().month
            }
            analysis_obj_list.append(analysis_obj)

        # Generating for total period
        total_on_lens = np.array(list(map(lambda obj: obj['on_len'], analysis_obj_list)))
        total_off_lens = np.array(list(map(lambda obj: obj['off_len'], analysis_obj_list)))

        # Generating for individual months
        grouped_on_lens = {}
        grouped_off_lens = {}
        for month in range(1, 12 + 1):
            month_obj_list = list(filter(lambda obj: obj['month'] == month, analysis_obj_list))
            month_on_lens = np.array(list(map(lambda obj: obj['on_len'], month_obj_list)))
            month_off_lens = np.array(list(map(lambda obj: obj['off_len'], month_obj_list)))
            grouped_on_lens[month] = month_on_lens
            grouped_off_lens[month] = month_off_lens

        # Reporting
        report_string = "Report" + (f" for {dataset_name_label} data.") if len(dataset_name_label) != 0 else ""
        report_string += f" {dataset_granularity_label} granularity." if len(dataset_granularity_label) != 0 else ""
        print(report_string+"\n")
        print(
            f"Total ON | len={len(total_on_lens)}, avg={np.average(total_on_lens)}, q1={np.percentile(total_on_lens, 25)}, q2={np.percentile(total_on_lens, 50)}, q3={np.percentile(total_on_lens, 75)}, min={np.min(total_on_lens)}, max={np.max(total_on_lens)}")
        print(
            f"Total OFF | len={len(total_off_lens)}, avg={np.average(total_off_lens)}, q1={np.percentile(total_off_lens, 25)}, q2={np.percentile(total_off_lens, 50)}, q3={np.percentile(total_off_lens, 75)}, min={np.min(total_off_lens)}, max={np.max(total_off_lens)}")

        if (minimum_usable_length is not None):
            def as_long(x):
                return x >= minimum_usable_length
            total_off_lens_usable = list(filter(as_long, total_off_lens))
            print(
                f"USABLE OFF | len={len(total_off_lens_usable)}, avg={np.average(total_off_lens_usable)}, q1={np.percentile(total_off_lens_usable, 25)}, q2={np.percentile(total_off_lens_usable, 50)}, q3={np.percentile(total_off_lens_usable, 75)}, min={np.min(total_off_lens_usable)}, max={np.max(total_off_lens_usable)}")
            print()

        print("For months: ")
        for month in range(1, 12 + 1):
            print(f"{month}:")
            grouped_on_lens_month = grouped_on_lens[month]
            grouped_off_lens_month = grouped_off_lens[month]

            print(
                f"\tON | len={len(grouped_on_lens_month)}, avg={np.average(grouped_on_lens_month)}, q1={np.percentile(grouped_on_lens_month, 25)}, q2={np.percentile(grouped_on_lens_month, 50)}, q3={np.percentile(grouped_on_lens_month, 75)}, min={np.min(grouped_on_lens_month)}, max={np.max(grouped_on_lens_month)}")
            print(
                f"\tOFF | len={len(grouped_off_lens_month)}, avg={np.average(grouped_off_lens_month)}, q1={np.percentile(grouped_off_lens_month, 25)}, q2={np.percentile(grouped_off_lens_month, 50)}, q3={np.percentile(grouped_off_lens_month, 75)}, min={np.min(grouped_off_lens_month)}, max={np.max(grouped_off_lens_month)}")

            if (minimum_usable_length is not None):
                def as_long(x):
                    return x >= minimum_usable_length
                grouped_on_lens_usable = list(filter(as_long, grouped_on_lens_month))
                grouped_off_lens_usable = list(filter(as_long, grouped_off_lens_month))

                print(f"---- USABLE (at least {minimum_usable_length} length) ---- ")
                if (len(grouped_on_lens_usable) != 0 and len(grouped_off_lens_usable) != 0):
                    # print(
                    #     f"\tON | len={len(grouped_on_lens_usable)}, avg={np.average(grouped_on_lens_usable)}, q1={np.percentile(grouped_on_lens_usable, 25)}, q2={np.percentile(grouped_on_lens_usable, 50)}, q3={np.percentile(grouped_on_lens_usable, 75)}, min={np.min(grouped_on_lens_usable)}, max={np.max(grouped_on_lens_usable)}")
                    print(
                    f"\tUSABLE OFF | len={len(grouped_off_lens_usable)}, avg={np.average(grouped_off_lens_usable)}, q1={np.percentile(grouped_off_lens_usable, 25)}, q2={np.percentile(grouped_off_lens_usable, 50)}, q3={np.percentile(grouped_off_lens_usable, 75)}, min={np.min(grouped_off_lens_usable)}, max={np.max(grouped_off_lens_usable)}")
                else:
                    print(f">>> No available intervals meeting the minimum usable length ({minimum_usable_length}) threshold <<<")


class RepoUtils:

    @classmethod
    def getRepoHash(cls):
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        return sha

class HeatPumpDatasetNYC:

    # Col definitions
    NEW_DATETIME_COL = 'DateTime'
    DATETIME_COL = NEW_DATETIME_COL
    AMBIENT_TEMP_COL = 'Ambient Outdoor Temperature'
    ROOM_TEMP_COL = 'Room Air Temperature'
    POWER_COL = "Total Unit Power"
    SITE_ID_COL = "Site Identification"
    DATE_COL = 'Date'
    TIME_COL = 'Time'

    @classmethod
    def site_subset_df(cls, df: pd.DataFrame, id: str) -> pd.DataFrame:
        # Method for selecting a specific site
        return df[df[cls.SITE_ID_COL] == id].drop(columns=[cls.SITE_ID_COL])


