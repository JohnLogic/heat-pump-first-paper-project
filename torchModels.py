# Re-loads all imports every time the cell is ran.
from utils import *

pd.options.display.float_format = '{:,.5f}'.format

# Sklearn tools

# Neural Networks
import torch
import torch.nn as nn

import pytorch_lightning as pl


class BaseRegressor(pl.LightningModule):
    '''
    Standard PyTorch Lightning module:
    https://pytorch-lightning.readthedocs.io/en/latest/lightning_module.html
    '''
    def __init__(self,
                 n_features,
                 batch_size,
                 dropout,
                 learning_rate,
                 criterion,
                 output_length = 4,
                 ):
        super(BaseRegressor, self).__init__()
        self.n_features = n_features
        self.batch_size = batch_size
        self.dropout = dropout
        self.criterion = criterion
        self.learning_rate = learning_rate
        self.output_length = output_length

        # self.hidden_size = hidden_size
        # self.num_layers = num_layers
        # self.seq_len = seq_len

        # TODO: add this to extending model
        # self.lstm = nn.LSTM(input_size=n_features,
        #                     hidden_size=hidden_size,
        #                     num_layers=num_layers,
        #                     dropout=dropout,
        #                     batch_first=True)
        # self.linear = nn.Linear(hidden_size, 4)

    # TODO: force to override this
    def forward(self, x):
        # lstm_out = (batch_size, seq_len, hidden_size)
        # lstm_out, other = self.lstm(x)
        # y_pred = self.linear(lstm_out[:,-1])
        # return y_pred
        raise NotImplementedError("Must implement in extending classes!")

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.learning_rate)

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.criterion(y_hat, y)
        # result = pl.TrainResult(loss)
        self.log('train_loss', loss, on_epoch=True, on_step=False)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.criterion(y_hat, y)
        # result = pl.EvalResult(checkpoint_on=loss)
        self.log('val_loss', loss, on_epoch=True, on_step=False)
        return loss

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.criterion(y_hat, y)
        # result = pl.EvalResult()
        self.log('test_loss', loss, on_epoch=True, on_step=False)
        return loss

class LSTMRegressor(BaseRegressor):
    def __init__(self,
                 n_features,
                 hidden_size,
                 seq_len,
                 batch_size,
                 num_layers,
                 dropout,
                 learning_rate,
                 criterion,
                 output_length):
        super(LSTMRegressor, self).__init__(
            n_features=n_features,
            batch_size=batch_size,
            dropout=dropout,
            learning_rate=learning_rate,
            criterion=criterion,
            output_length=output_length
        )
        self.hidden_size = hidden_size
        self.seq_len = seq_len
        self.num_layers = num_layers
        # self.n_features = n_features
        # self.batch_size = batch_size
        # self.dropout = dropout
        # self.criterion = criterion
        # self.learning_rate = learning_rate

        self.lstm = nn.LSTM(input_size=n_features,
                            hidden_size=hidden_size,
                            num_layers=num_layers,
                            dropout=dropout,
                            batch_first=True)
        self.linear = nn.Linear(hidden_size, self.output_length)

    def forward(self, x):
        # lstm_out = (batch_size, seq_len, hidden_size)
        lstm_out, other = self.lstm(x)
        y_pred = self.linear(lstm_out[:, -1])
        return y_pred

class LSTMRegressorRoundDown(LSTMRegressor):

    def forward(self, x):
        return torch.floor(super().forward(x))

class GRURegressor(BaseRegressor):
    def __init__(self,
                 n_features,
                 hidden_size,
                 seq_len,
                 batch_size,
                 num_layers,
                 dropout,
                 learning_rate,
                 criterion):
        super(GRURegressor, self).__init__(
            n_features=n_features,
            batch_size=batch_size,
            dropout=dropout,
            learning_rate=learning_rate,
            criterion=criterion
        )
        self.hidden_size = hidden_size
        self.seq_len = seq_len
        self.num_layers = num_layers
        self.lstm = nn.GRU(input_size=n_features,
                            hidden_size=hidden_size,
                            num_layers=num_layers,
                            dropout=dropout,
                            batch_first=True)
        self.linear = nn.Linear(hidden_size, self.output_length)

    def forward(self, x):
        gru_out, other = self.lstm(x)
        y_pred = self.linear(gru_out[:, -1])
        return y_pred

class LSTMResidualRegressor(LSTMRegressor):
    def forward(self, x):
        y_pred = super().forward(x)
        init_x = torch.repeat_interleave(x[:, -1, 0], self.output_length).reshape(-1, self.output_length)
        y_pred_res = torch.add(init_x, y_pred)
        return y_pred_res

class LinearRegressor(BaseRegressor):
    def __init__(self,
                 n_features,
                 batch_size,
                 dropout,
                 learning_rate,
                 criterion
                 ):
        super(LinearRegressor, self).__init__(n_features, batch_size, dropout, learning_rate, criterion)
        self.linearRegressors = nn.ModuleList(
            [nn.Linear(in_features=self.n_features, out_features=1) for _ in range(self.output_length)]
        )


    def forward(self, x):
        # Use -1 because we want flexible batch_sizes
        out_tensor = self.linearRegressors[0](x).reshape(-1, 1)
        for regressor_idx in range(1, len(self.linearRegressors)):
            # NOTE: the extra ( ) pair
            prediction = self.linearRegressors[regressor_idx](x).reshape(-1, 1)
            out_tensor = torch.cat( (out_tensor, prediction),1 )
        return out_tensor

class LinearRegressorAlt(BaseRegressor):
    def __init__(self,
                 n_features,
                 batch_size,
                 dropout,
                 learning_rate,
                 criterion,
                 output_length
                 ):
        super(LinearRegressorAlt, self).__init__(n_features, batch_size, dropout, learning_rate, criterion)
        self.linearRegressor = nn.Linear(in_features=self.n_features, out_features=4)
        self.output_length = self.output_length


    def forward(self, x):
        # Use -1 because we want flexible batch_sizes
        out_tensor = self.linearRegressor(x).reshape(-1, self.output_length)
        return out_tensor