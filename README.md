## Dependencies

- Anaconda (first)

- Pandas, Matplotlib, Numpy
```bash
conda install pandas, matplotlib, numpy
```

- NeuralCDE
```bash
pip install git+https://github.com/patrick-kidger/NeuralCDE.git
```

- Pytorch without CUDA, MacOS
```bash
conda install pytorch torchvision torchaudio -c pytorch
```

- PyTorch forecasting
```bash
conda install -c conda-forge pytorch-forecasting
```

- Latest version of PyTorch lightning
```bash
conda install -c conda-forge pytorch-lightning
```

- asd 
 ```bash
conda install gitpython
```

- asd TODO: test this out
```
pip3 install pytorchts --use-feature=2020-resolver
conda install seaborn
conda install pandas=1.0.5 <<< doesn't work
```


## Notes for development

- Preprocessing change?
    - Go to ``torchUtils.py``
        - Test dm method