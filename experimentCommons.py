from typing import Optional

import torch
import torch.nn as nn

import torchMetrics
import torchModels
import torchUtils

# Column definitions
NEW_DATETIME_COL = 'DateTime'
DATETIME_COL = NEW_DATETIME_COL # for compat with old prepDataNYC NBook
AMBIENT_TEMP_COL = 'Ambient Outdoor Temperature'
ROOM_TEMP_COL = 'Room Air Temperature'
POWER_COL = "Total Unit Power"
SITE_ID_COL = "Site Identification"
DATE_COL = 'Date'
TIME_COL = 'Time'

# ----------[ Loss foo selection ]----------

def select_loss_map (param_dict: dict):
    loss_map = {
        "mse": nn.MSELoss(),
        "rmse": torchMetrics.RMSELoss(),
    }
    return loss_map


def select_model_map (param_dict: dict):
    model_map = {
        "torch_lstm": torchModels.LSTMRegressor(
            n_features = param_dict["n_features"],
            hidden_size= param_dict["hidden_size"],
            seq_len=param_dict["seq_len"],
            batch_size=param_dict["batch_size"],
            num_layers=param_dict["num_layers"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],
            output_length=param_dict["horizon_length"]
        ),
        "torch_gru": torchModels.GRURegressor(
            n_features=param_dict["n_features"],
            hidden_size=param_dict["hidden_size"],
            seq_len=param_dict["seq_len"],
            batch_size=param_dict["batch_size"],
            num_layers=param_dict["num_layers"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],
        ),
        "residual_torch_lstm": torchModels.LSTMResidualRegressor(
            n_features = param_dict["n_features"],
            hidden_size= param_dict["hidden_size"],
            seq_len=param_dict["seq_len"],
            batch_size=param_dict["batch_size"],
            num_layers=param_dict["num_layers"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],
            output_length=param_dict["horizon_length"],
        ),
        # "basic_tcn": contribModels.TCNRegressor(
        #     num_inputs=param_dict["n_features"],
        #     input_size=param_dict["n_features"], # ???
        #     num_channels=-1,
        #     batch_size=param_dict["batch_size"],
        #     learning_rate=param_dict["learning_rate"],
        #     criterion=param_dict["loss"],
        #     kernel_size=2,
        #     dropout=param_dict["dropout"],
        # ),
        "lr": torchModels.LinearRegressor(
            # TODO: calculate n_features
            # memory_length=int(ARG_MEMORY_LENGTH),
            # horizon_length=int(ARG_HORIZON_LENGTH),
            n_features=(param_dict["memory_length"]) * 3 + \
                       (6 if bool(param_dict["add_time_features"]) else 0) + \
                       param_dict["horizon_length"],
            batch_size=param_dict["batch_size"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],
        ),
    }
    return model_map

def load_model_from_checkpoint(checkpoint_path: str, param_dict: dict):
    if (param_dict['model_name'] == 'torch_lstm'):
        return torchModels.LSTMRegressor.load_from_checkpoint(
                checkpoint_path=checkpoint_path,
                n_features = param_dict["n_features"],
                hidden_size= param_dict["hidden_size"],
                seq_len=param_dict["seq_len"],
                batch_size=param_dict["batch_size"],
                num_layers=param_dict["num_layers"],
                dropout=param_dict["dropout"],
                learning_rate=param_dict["learning_rate"],
                criterion=param_dict["loss"],
                output_length=param_dict["horizon_length"],
            )
    elif (param_dict['model_name'] == 'residual_torch_lstm'):
        return torchModels.LSTMResidualRegressor.load_from_checkpoint(
            checkpoint_path=checkpoint_path,
            n_features = param_dict["n_features"],
            hidden_size= param_dict["hidden_size"],
            seq_len=param_dict["seq_len"],
            batch_size=param_dict["batch_size"],
            num_layers=param_dict["num_layers"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],
            output_length=param_dict["horizon_length"],
        )
    elif (param_dict['model_name'] == 'torch_gru'):
        return torchModels.GRURegressor.load_from_checkpoint(
            checkpoint_path=checkpoint_path,
            n_features = param_dict["n_features"],
            hidden_size= param_dict["hidden_size"],
            seq_len=param_dict["seq_len"],
            batch_size=param_dict["batch_size"],
            num_layers=param_dict["num_layers"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"]
        )
    elif (param_dict['model_name'] == 'lr'):
        return torchModels.LinearRegressor.load_from_checkpoint(
            checkpoint_path=checkpoint_path,
            # TODO: calculate n_features
            # memory_length=int(ARG_MEMORY_LENGTH),
            # horizon_length=int(ARG_HORIZON_LENGTH),
            n_features=(param_dict["memory_length"]) * 3 + \
                       (6 if bool(param_dict["add_time_features"]) else 0) + \
                       param_dict["horizon_length"],
            batch_size=param_dict["batch_size"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],
        )

    elif (param_dict['model_name'] == 'lr_alt'):
        raise ValueError("Don't use 'lr_alt'!")
        return torchModels.LinearRegressor.load_from_checkpoint(
            # TODO: calculate n_features
            # memory_length=int(ARG_MEMORY_LENGTH),
            # horizon_length=int(ARG_HORIZON_LENGTH),
            checkpoint_path=checkpoint_path,
            n_features=(param_dict["memory_length"]) * 3 + \
                       (6 if bool(param_dict["add_time_features"]) else 0) + \
                       param_dict["horizon_length"],
            batch_size=param_dict["batch_size"],
            dropout=param_dict["dropout"],
            learning_rate=param_dict["learning_rate"],
            criterion=param_dict["loss"],

            # These three below are necessary for compat. reasons
            hidden_size=param_dict["hidden_size"],
            seq_len=param_dict["seq_len"],
            num_layers=param_dict["num_layers"],
        )


def get_data_module(type,
                    param_dict: dict,
                    override_batch_size : Optional[int] = None,
                    input_type: str = "normal",
                    override_memory_length : Optional[int] = None,
                    no_weekend: bool = False,
                    ):

    # input_type == normal -> regular structure
    #            == flat -> flat structure

    if (override_batch_size is None):
        override_batch_size = param_dict['batch_size']
    memory_length = param_dict['memory_length']

    flatten_xs = True if param_dict['input_type'] == 'flat' else False
    if (type == "full"):
        return torchUtils.FullDataModule(
            dataset_path=param_dict["data_dir"],
            seq_len=param_dict['seq_len'],
            batch_size=override_batch_size,  # By default takes from 'param_dict'
            site_ids=param_dict['site_ids'],
            datetime_col=DATETIME_COL,
            power_col=POWER_COL,
            indoor_temp_col=ROOM_TEMP_COL,
            outdoor_temp_col=AMBIENT_TEMP_COL,
            idle_power_threshold=param_dict['power_threshold'],
            horizon_length=param_dict['horizon_length'],
            memory_length=memory_length,
            add_time_features=True,
            flatten_xs=flatten_xs,
            override_memory_length=override_memory_length,
            remove_weekends_from_train_and_val=no_weekend,
        )
    elif (type == "interval"):
        return torchUtils.TemperaturePowerIntervalDataModule(
            dataset_path = param_dict["data_dir"],
            seq_len = param_dict['seq_len'],
            batch_size = override_batch_size, # By default takes from 'param_dict'
            site_ids = param_dict['site_ids'],
            datetime_col = DATETIME_COL,
            power_col = POWER_COL,
            indoor_temp_col = ROOM_TEMP_COL,
            outdoor_temp_col = AMBIENT_TEMP_COL,
            idle_power_threshold = param_dict['power_threshold'],
            horizon_length = param_dict['horizon_length'],
            memory_length =memory_length,
            add_time_features = param_dict['add_time_features'],
            flatten_xs = flatten_xs,
        )
    else:
        raise ValueError(f"Type '{type}' dataloader is unsupported!")

def get_appropriate_input_type(model_name: str) -> str:
    # Select the data structure (flat vs normal), depends on the model
    input_type = None
    if (model_name in ['lr', ...]):
        input_type = 'flat'
    else:
        input_type = 'normal'
    return input_type

def get_site_ids_from_string(site_id_string:str) -> [str]:
    return list(map(lambda s: s.strip(), site_id_string.split(',')))

# Method for transforming X_batch from flat format to proper
def get_proper_X(inverse_transformed_X_batch,
                 memory_length: int,
                 input_type: str,
                 add_zero_power_signal = False):
    X_batch_proper = torch.tensor(inverse_transformed_X_batch) # Normalize BEFORE
    #     (param_dict['memory_length']*3) + 6 + param_dict['horizon_length']
    if (input_type == 'flat'):
        shape = X_batch_proper.shape # 1,1,N
        reshaped = X_batch_proper.reshape(-1)
        memory_part = reshaped[0:(memory_length*3)]
        proper_memory_part = memory_part.reshape(-1, 3)
        static_part = reshaped[(memory_length*3):]
        repeated_static_part = static_part.repeat(memory_length)
        proper_static_part = repeated_static_part.reshape(-1, len(static_part))
        X_batch_proper = torch.cat((proper_memory_part, proper_static_part), 1).reshape([1,memory_length, -1])
    if (add_zero_power_signal): # This is for the interval dataset
        zeros = torch.zeros([1, memory_length, 4])
        X_batch_proper = torch.cat((X_batch_proper, zeros), 2)
    return X_batch_proper