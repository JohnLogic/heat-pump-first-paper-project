# Contributions

This folder contains contributed repos/libraries from other authors. 

1. NeuralCDE repository: https://github.com/patrick-kidger/NeuralCDE

## Script

```bash
git clone https://github.com/patrick-kidger/NeuralCDE.git
```
