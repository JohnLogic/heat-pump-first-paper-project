# For working with Pandas dataframes
import copy
import re


def pick_columns(cols: [str], cols_to_pick: [str]) -> [str]:
    # TODO: consider using list comprehensions here
    picked_columns = []
    for col_to_pick in cols_to_pick:
        picked_columns += list(filter(lambda col: col_to_pick in col, cols))
    return picked_columns


def ignore_columns(cols: [str], cols_to_ignore: [str]) -> [str]:
    # TODO: consider using list comprehensions here
    picked_columns = []
    for col_to_ignore in cols_to_ignore:
        picked_columns += list(filter(lambda col: col_to_ignore not in col, cols))
    return picked_columns

def natural_sort(list: [str]) -> [str]:
    """ Sort the given list in the way that humans expect.
    """
    copied_list = copy.deepcopy(list)
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    copied_list.sort(key=alphanum_key)
    return copied_list

# TODO: consider what to do with useful code snippets
def _example_extract_datetime():
    from datetime import datetime
    example_dt_string = "05/02/2015,18:15:00"
    example_dt = datetime.strptime(example_dt_string, "%m/%d/%Y,%H:%M:%S")
    return example_dt_string, example_dt