# Re-loads all imports every time the cell is ran.
import functools

from utils import *

# %load_ext autoreload
# %autoreload 2
pd.options.display.float_format = '{:,.5f}'.format

# Sklearn tools
from sklearn.preprocessing import StandardScaler

# Neural Networks
import torch

from torch.utils.data import Dataset, DataLoader

import pytorch_lightning as pl
from pytorch_lightning.callbacks import Callback


#%%

def add_features_to_interval(interval_df: pd.DataFrame) -> pd.DataFrame:
    # day
    date_col = interval_df.index
    interval_df['Day'] = date_col.day
    # month
    interval_df['Month'] = date_col.month
    # year
    interval_df['Year'] = date_col.year
    # hour
    interval_df['Hour'] = date_col.hour
    # minute
    interval_df['Minute'] = date_col.minute
    # Monday is 0 and Sunday is 6
    interval_df['Weekday'] = date_col.weekday
    return interval_df

def prepareNYCData(nyc_data_path: str ,
                   site_ids: [str],
                   datetime_col: str,
                   power_col: str,
                   indoor_temp_col: str,
                   outdoor_temp_col: str,
                   idle_power_threshold: float,
                   horizon_length: int,
                   memory_length: int,
                   add_time_features: bool
                   ) -> [pd.DataFrame]:
    path_obj = Path(nyc_data_path)
    absolute_df_path_str = str(path_obj.resolve())

    # Add memory to trimmed dataframes
    # Use the min index and simply append
    # TODO: Might need to omit some of the earlier ones, as they might not fit
    def add_memory_to_interval(interval_df: pd.DataFrame,
                               full_df: pd.DataFrame,
                               memory_size: int = memory_length,
                               target_size: int = horizon_length
                               ) -> pd.DataFrame:
        # Params
        # memory_size = memory_length
        # target_size = horizon_length
        # full_df = site_43_df
        # interval_df = trimmed_intervals[1]

        first_index = interval_df.index.min()
        first_record = full_df.loc[first_index]
        first_interval_record = interval_df.loc[first_index]

        # print(f"Interval_df: {first_interval_record}")
        # print()
        # print(f"First record: {first_record}")

        # Sanity check to see if we always have the same record
        if not ((first_interval_record.loc[power_col] == first_record.loc[power_col]) and (
                first_interval_record.loc[indoor_temp_col] == first_record.loc[indoor_temp_col]) and (
                        first_interval_record.loc[outdoor_temp_col] == first_record.loc[outdoor_temp_col])):
            raise Exception(f"{first_interval_record}\n != {first_record}")

        current_memory_count = (len(interval_df) - target_size)
        first_index_numeric = full_df.index.get_loc(first_index)

        proper_interval_df = interval_df
        if (current_memory_count > memory_size):
            tail_size = memory_size + target_size
            proper_interval_df = proper_interval_df.tail(tail_size)
        elif (current_memory_count < memory_size):
            missing_memory_count = memory_size - current_memory_count
            missing_frame = full_df.iloc[first_index_numeric - missing_memory_count:first_index_numeric]
            proper_interval_df = pd.concat([missing_frame, interval_df])

        return proper_interval_df  # <<< RETURN

    # Sanity check - file is located where it should be
    if (not path_obj.exists()):
        raise Exception(f"NYC data .csv file does not exist in directory: {path_obj.resolve()}")

    if (len(site_ids) == 0):
        raise Exception("Site ids have to be specified!")

    nyc_data_df = pd.read_csv(absolute_df_path_str,
                              index_col=datetime_col,
                              parse_dates=True)


    proper_intervals = []
    for site_id in site_ids:

        # TODO: add a for loop here (iterate through site_ids)

        ### Preparing speficic site data
        site_df = HeatPumpDatasetNYC.site_subset_df(nyc_data_df, site_id)

        ### Fixing data
        DataPreparationUtils.outlier_fix(site_df,
                                         indoor_temp_col,
                                         outdoor_temp_col,
                                         outlier_temp_threshold=15.)  # Main

        ### After outliers are removed - interpolation on temperatures is performed
        site_df = site_df.resample('15min').asfreq()
        site_df[indoor_temp_col] = site_df[indoor_temp_col].interpolate()
        site_df[outdoor_temp_col] = site_df[outdoor_temp_col].interpolate()

        site_interval_list = DataPreparationUtils.split_data_into_intervals(df=site_df,
                                                                            minimum_interval_length=4,
                                                                            maximum_interval_length=1000000,
                                                                            max_idle_power=idle_power_threshold,
                                                                            power_col_name=power_col
                                                                            )

        # Check if intervals contain NaNs
        # TODO: remove those which do, if they come up
        # TODO: why doesn't this properly work?
        found_nan_intervals = 0
        for interval_df in site_interval_list:
            if (interval_df.isnull().sum().max() > 0):
                found_nan_intervals += 1
                print(interval_df)

        print("[ All intervals have no NaNs ]") if found_nan_intervals == 0 else print(
            f"Found {found_nan_intervals} NaN intervals")

        # Select intervals who have OFF/LOW power intervals of at least 4
        # Everything else has to be cut to fit 4

        enough_off_intervals = (list(filter(lambda interval_df: len(
            interval_df[interval_df[power_col] <= idle_power_threshold]) >= horizon_length,
                                            site_interval_list)))
        print(f"Enough off intervals: {len(enough_off_intervals)}")

        def trim_interval_to_off_size(interval_df: pd.DataFrame,
                                      to_off_length: int = horizon_length) -> pd.DataFrame:
            off_length = len(interval_df[interval_df[power_col] <= idle_power_threshold])
            if (off_length > to_off_length):
                trim_length = off_length - to_off_length
                return interval_df.head(len(interval_df) - trim_length)
            else:
                return interval_df

        trimmed_intervals = list(map(lambda interval_df: trim_interval_to_off_size(interval_df, horizon_length),
                                     enough_off_intervals))
        list_of_trimmed_lengths = list(
            map(lambda interval_df: len(interval_df[interval_df[power_col] <= idle_power_threshold]), trimmed_intervals))
        if not (max(list_of_trimmed_lengths) == min(list_of_trimmed_lengths) == horizon_length):
            raise Exception("Interval trim algorithm doesn't work.")
        else:
            print(
                f"Trimming is ok! max={max(list_of_trimmed_lengths)} == min={min(list_of_trimmed_lengths)} == int={horizon_length}")
        print(f"Trimmed intervals: {len(trimmed_intervals)}")

        list(filter(lambda length: length != horizon_length, list_of_trimmed_lengths))


        intervals = list(map(
            lambda interval_df: add_memory_to_interval(
                interval_df, site_df,
                memory_size=memory_length,
                target_size=horizon_length), trimmed_intervals))

        # This is for filtering the shorter intervals (with not enough memory)
        filtered_intervals = list(filter(lambda interval_df: len(interval_df) == memory_length + horizon_length,
                                    intervals))

        interval_lengths = list(map(lambda df: len(df), filtered_intervals))
        if not (min(interval_lengths) == max(interval_lengths) == memory_length + horizon_length):
            # TODO: update if we add more features
            raise Exception("Intervals are not of proper length...")
        else:
            print("Intervals are prepared for training :)")

        if (add_time_features):


            filtered_intervals = list(map(lambda idf: add_features_to_interval(idf), filtered_intervals))

        proper_intervals += filtered_intervals

    return proper_intervals

def _cached_interval_file_path(dataset_alias: str, site_ids: [str], memory: int, horizon:int, features:int) -> str:
    sha = RepoUtils.getRepoHash()
    site_ids_string = functools.reduce(lambda acc, site: acc + site, ListUtils.natural_sort(site_ids), "")
    return os.path.join("output", f"{dataset_alias}_{site_ids_string}_{memory}_{horizon}_{features}_{sha}.csv")

def load_intervals_from_csv(dataset_alias:str,
                            datetime_col:str,
                            interval_idx_col:str,
                            site_ids:[str],
                            memory: int,
                            horizon: int,
                            feature_count: int
                            ) -> [pd.DataFrame]:
    absolute_df_path_str = _cached_interval_file_path(dataset_alias,
                                                      site_ids,
                                                      memory,
                                                      horizon,
                                                      feature_count)
    interval_df = pd.read_csv(absolute_df_path_str,
                                  index_col=datetime_col,
                                  parse_dates=True)
    count_of_intervals = interval_df[interval_idx_col].max() + 1
    interval_df = interval_df.drop([interval_idx_col], axis=1)
    splits = np.array_split(interval_df, count_of_intervals)
    return splits

def save_intervals_to_df(proper_intervals: [pd.DataFrame],
                         datetime_col: str,
                         interval_idx_col: str,
                         dataset_alias: str,
                         site_ids: [str],
                         memory: int,
                         horizon: int,
                         feature_count: int):
    print("Started caching generated intervals!")
    modified_proper_intervals = []
    for idx, proper_interval in enumerate(proper_intervals):
        modified_proper_interval = proper_interval.copy(deep=True)
        modified_proper_interval[interval_idx_col] = idx
        modified_proper_intervals.append(modified_proper_interval)

    all_proper_interval_df = pd.concat(modified_proper_intervals)

    # NOTE: adds hash that acts as a key to check if version changed (so we don't recalc the dataset everytime!)
    csv_file_path = _cached_interval_file_path(dataset_alias=dataset_alias,
                                               site_ids=site_ids,
                                               memory=memory,
                                               horizon=horizon,
                                               features=feature_count)
    all_proper_interval_df.to_csv(
        csv_file_path,
        index_label=datetime_col,
        header=True)

    if (Path(csv_file_path).exists()):
        print(f"{dataset_alias} saved generated intervals successfully!")
    else:
        raise IOError(f"Failed to save the file in path {Path(csv_file_path).absolute()}")

class TemperaturePowerIntervalDataModule(pl.LightningDataModule):
    '''
    PyTorch Lighting DataModule subclass:
    https://pytorch-lightning.readthedocs.io/en/latest/datamodules.html

    Serves the purpose of aggregating all data loading
      and processing work in one place.
    '''

    # TODO: remove seq_len
    def __init__(self,
                 dataset_path: str,
                 batch_size: int,
                 seq_len: Optional[int],
                 site_ids: [str],
                 datetime_col: str,
                 power_col: str,
                 indoor_temp_col: str,
                 outdoor_temp_col: str,
                 idle_power_threshold: float,
                 horizon_length: int,
                 memory_length: int,
                 dataset_alias: str = "NYC_intervals",
                 train_test_split_count: int = 8,
                 train_val_split_count: int = 8,
                 add_time_features: bool = False,
                 flatten_xs: bool = False,
                 ):
        super().__init__()
        self.dataset_path = dataset_path
        self.seq_len = seq_len
        self.site_ids = site_ids
        self.datetime_col = datetime_col
        self.power_col = power_col
        self.indoor_temp_col = indoor_temp_col
        self.outdoor_temp_col = outdoor_temp_col
        self.idle_power_threshold = idle_power_threshold
        self.horizon_length = horizon_length
        self.memory_length = memory_length
        self.dataset_alias = dataset_alias
        self.interval_idx_col = "interval_idx"

        self.batch_size = batch_size
        self.num_workers = 0 # TODO: consider making this more???
        self.X_train = None
        self.y_train = None
        self.X_val = None
        self.y_val = None
        self.X_test = None
        self.X_test = None
        self.columns = None
        self.preprocessing = None
        self.scaler = None

        self.train_test_split_count = train_test_split_count
        self.train_val_split_count = train_val_split_count
        self.add_time_features = add_time_features
        self.feature_count = 9 if self.add_time_features else 3
        self.flatten_xs = flatten_xs

    def prepare_data(self):
        pass

    def setup(self, stage=None):
        # NOTE: stage == 'fit' -> train/val datasets
        #       stage == 'test' -> test datasets

        # Check if setup required, if not -> skip
        if stage == 'fit' and self.X_train is not None:
            return
        if stage == 'test' and self.X_test is not None:
            return
        if stage is None and self.X_train is not None and self.X_test is not None:
            return

        proper_splits: [pd.DataFrame]
        cached_interval_file_path_obj = Path(_cached_interval_file_path(dataset_alias=self.dataset_alias,
                                                                        site_ids=self.site_ids,
                                                                        memory=self.memory_length,
                                                                        horizon=self.horizon_length,
                                                                        features=self.feature_count))
        if ( cached_interval_file_path_obj.exists()):
            print(f"Found cached interval file in {cached_interval_file_path_obj.absolute()}")
            proper_splits = load_intervals_from_csv(dataset_alias=self.dataset_alias,
                                    datetime_col=self.datetime_col,
                                    interval_idx_col=self.interval_idx_col,
                                    site_ids=self.site_ids,
                                    memory=self.memory_length,
                                    horizon=self.horizon_length,
                                    feature_count=self.feature_count)
        else:
            splits = prepareNYCData(
                nyc_data_path=self.dataset_path,
                site_ids=self.site_ids,
                datetime_col=self.datetime_col,
                power_col=self.power_col,
                indoor_temp_col=self.indoor_temp_col,
                outdoor_temp_col=self.outdoor_temp_col,
                idle_power_threshold=self.idle_power_threshold,
                horizon_length=self.horizon_length,
                memory_length=self.memory_length,
                add_time_features=self.add_time_features,
            )

            for idx, split in enumerate(splits):
                if split.isnull().values.any():
                    split.fillna(method='ffill', inplace=True)
                    print(f"Split idx: {idx} has NaNs")

            # Remove all splits which have NaNs, even after ffilling
            # TODO: preprocess NaNs earlier
            proper_splits = list(filter(lambda split: not split.isnull().values.any(), splits))
            save_intervals_to_df(proper_splits,
                                 datetime_col=self.datetime_col,
                                 interval_idx_col=self.interval_idx_col,
                                 dataset_alias=self.dataset_alias,
                                 site_ids=self.site_ids,
                                 memory=self.memory_length,
                                 horizon=self.horizon_length,
                                 feature_count=self.feature_count)

        splits_array = np.stack(proper_splits)

        cv_list, test_list = DataPreparationUtils.every_n_train_test_split(splits_array, self.train_test_split_count)
        train_list, val_list = DataPreparationUtils.every_n_train_test_split(cv_list, self.train_val_split_count)

        print(f"train: {len(train_list)}, val: {len(val_list)}, test: {len(test_list)}")

        train_array, val_array, test_array = np.array(train_list), np.array(val_list), np.array(test_list)

        def grab_X_from_interval(interval_arr: np.array, y_length: int = self.horizon_length) -> np.array:
            return interval_arr[:-y_length]
        def grab_y_from_interval(interval_arr: np.array, y_length: int = self.horizon_length) -> np.array:
            return interval_arr[-y_length:]

        X_train = np.array(list(map(lambda interval: grab_X_from_interval(interval), train_array)))
        X_val = np.array(list(map(lambda interval: grab_X_from_interval(interval), val_array)))
        X_test = np.array(list(map(lambda interval: grab_X_from_interval(interval), test_array)))

        y_train = np.array(list(map(lambda interval: grab_y_from_interval(interval), train_array)))
        y_val = np.array(list(map(lambda interval: grab_y_from_interval(interval), val_array)))
        y_test = np.array(list(map(lambda interval: grab_y_from_interval(interval), test_array)))

        def flatten_out(X: np.array) -> np.array:
            static_values = X[0, 4:len(X)] # Caching only ONE sample of the static values
            ts_values = X[:, 0:3].reshape(-1) #Flattening out the values
            return np.append(ts_values, static_values)

        if (self.flatten_xs):
            X_train = np.array(list(map(lambda x: flatten_out(x), X_train)))
            X_val = np.array(list(map(lambda x: flatten_out(x), X_val)))
            X_test = np.array(list(map(lambda x: flatten_out(x), X_test)))

        self.scaler = StandardScaler() # TODO: consider alternative scalers, as well
        self.scaler.fit(np.vstack(X_train)) # NOTE: vstack makes one giant array along the 0th dimension (rows)

        # vstack and split   ---   np.array_split

        # 2. select last column of all Ys

        # x train_array[0][-4:]
        # y train_array[0][:-4]

        # X_cv, X_test, y_cv, y_test = train_test_split(
        #     X, y, test_size=0.2, shuffle=False
        # )
        #
        # X_train, X_val, y_train, y_val = train_test_split(
        #     X_cv, y_cv, test_size=0.25, shuffle=False
        # )

        def select_future_temps_from_y(y_arr: np.array) -> np.array:
            # TODO: 0 depends on the order of the dataframe, consider extracting this from dataframe
            return y_arr[:,0]

        if stage == 'fit' or stage is None:
            self.X_train = np.array_split(self.scaler.transform(np.vstack(X_train)), len(X_train))
            self.y_train = np.array([select_future_temps_from_y(y) for y in y_train])
            self.y_train = self.y_train.reshape((-1, self.horizon_length))

            self.X_val = np.array_split(self.scaler.transform(np.vstack(X_val)), len(X_val))
            self.y_val = np.array([select_future_temps_from_y(y) for y in y_val])
            self.y_val = self.y_val.reshape((-1, self.horizon_length))

            DataPreparationUtils.fail_if_nan(self.X_train)
            DataPreparationUtils.fail_if_nan(self.X_val)
            DataPreparationUtils.fail_if_nan(self.y_train)
            DataPreparationUtils.fail_if_nan(self.y_val)

        # NOTE: we only do dataprep during 'fit'
        # if stage == 'test' or stage is None:
            self.X_test = np.array_split(self.scaler.transform(np.vstack(X_test)), len(X_test))
            self.y_test = np.array([select_future_temps_from_y(y) for y in y_test])
            self.y_test = self.y_test.reshape((-1, self.horizon_length))
            DataPreparationUtils.fail_if_nan(self.X_test)
            DataPreparationUtils.fail_if_nan(self.y_test)

        print("Setup() done")


    def train_dataloader(self):
        train_dataset = CustomIntervalDataset(
            self.X_train, self.y_train, )
        train_loader = DataLoader(train_dataset,
                                  batch_size = self.batch_size,
                                  shuffle = False,
                                  num_workers = self.num_workers)

        return train_loader

    def val_dataloader(self):
        val_dataset = CustomIntervalDataset(
            self.X_val, self.y_val, )
        val_loader = DataLoader(val_dataset,
                                batch_size = self.batch_size,
                                shuffle = False,
                                num_workers = self.num_workers)

        return val_loader

    def test_dataloader(self):
        test_dataset = CustomIntervalDataset(
            self.X_test, self.y_test,)
        test_loader = DataLoader(test_dataset,
                                 batch_size = self.batch_size,
                                 shuffle = False,
                                 num_workers = self.num_workers)

        return test_loader

#%%

class CustomIntervalDataset(Dataset):
    '''
    Custom Dataset subclass.
    Serves as input to DataLoader o transform X
      into sequence data using rolling window.
    DataLoader using this dataset will output batches
      of `(batch_size, seq_len, n_features)` shape.
    Suitable as an input to RNNs.
    '''
    # TODO: remove seq_len
    def __init__(self, X: np.ndarray, y: np.ndarray, seq_len: Optional[int] = None):
        # TODO: is the .float necessary here
        self.X = torch.tensor(X).float()
        self.y = torch.tensor(y).float()

        # Sanity check
        if not (X.__len__() == y.__len__()):
            raise ValueError("Passed X and y lengths are not equal!")
        # self.seq_len = seq_len # Seq length isn't really required anymore

    def __len__(self):
        # return self.X.__len__() - (self.seq_len-1)
        return self.X.__len__() # basically default

    def __getitem__(self, index):
        # return (self.X[index:index+self.seq_len], self.y[index+self.seq_len-1])
        return (self.X[index], self.y[index]) # basically default

# TODO: deprecated / not working attempt to separating logging away from the pl module
class MyCallback(Callback):

    def _log(self, pl_module, callback_metrics, metric):
        if (callback_metrics):
            pl_module.log(metric, callback_metrics[metric])

    def on_validation_epoch_end(self, trainer, pl_module):
        # pl_module.log('val_loss', callback_metrics['val_loss'])
        callback_metrics = trainer.callback_metrics
        self._log(pl_module, callback_metrics, 'val_loss')
        # callback_metrics2 = trainer.callback_metrics
        print(callback_metrics)

    def on_train_epoch_end(self, trainer, pl_module, outputs):
        callback_metrics = trainer.callback_metrics
        self._log(pl_module, callback_metrics, 'train_loss')
        print(callback_metrics)

    def on_test_end(self, trainer, pl_module):
        callback_metrics = trainer.callback_metrics
        self._log(pl_module, callback_metrics, 'test_loss')
        print(callback_metrics)

####################################################################

# Returns: train_parts, val_parts, test_parts
def partition_df(
    site_df: pd.DataFrame,
    train_block_size_in_days: int = 16,
    val_block_size_in_days: int = 2,
    test_block_size_in_days:int = 2,
    single_block_size:int = 96,  # 1 day in 15 min
):
    mode_blocks = [
        train_block_size_in_days * single_block_size,
        val_block_size_in_days * single_block_size,
        test_block_size_in_days * single_block_size,
    ]

    mode_dfs = [ [], [], [] ]
    idx, mode, site_df_len = 0, 0, len(site_df)

    while (idx < site_df_len):
        step = mode_blocks[mode]
        mode_dfs[mode].append(site_df.iloc[idx:idx + step])
        mode = (mode + 1) % 3
        idx += step

    return mode_dfs

def add_power_vector_to_xy(xy_df: pd.DataFrame,
                           power_col: str,
                           memory_length: int,
                           horizon_length: int):
    for horizon_i in range(horizon_length):
        xy_df[power_col+f"{horizon_i+1}"] = xy_df[power_col].iloc[memory_length+horizon_i]
    return xy_df

def generate_xy_from_partition(partition: pd.DataFrame,
                                power_col: str,
                                memory_length: int,
                                horizon_length: int):
    partition_len = len(partition)
    xy_length = memory_length + horizon_length
    # TODO: add future power features!
    # https://datascience.stackexchange.com/questions/17099/adding-features-to-time-series-model-lstm
    return [add_power_vector_to_xy(partition[idx: idx + xy_length],
                                   power_col=power_col,
                                   memory_length=memory_length,
                                   horizon_length=horizon_length,)
            for idx in range(partition_len - xy_length)]

# test_xy = generate_xy_from_partition(test_partition, )

    # This way is SUUUUPER slow at least in Pycharm
    # return [
    #     (partition[idx: idx+memory_length], # Selects all the Xs
    #      partition[idx+memory_length: idx+memory_length+horizon_length]) # Selects Ys
    #     for idx in range(partition_len-(memory_length+horizon_length))
    # ]

    # idx, xy_list = 0, []
    # len_df = len(partition)
    # while (idx+xy_length < len_df):
    #     xy_list.append(partition[idx: idx+xy_length])
    #     idx+=1
    # pass

def prepare_full_data(
        absolute_df_path_str: str = "./output/NYC_data_15min.csv",
        datetime_col: str = "DateTime",
        indoor_temp_col: str = 'Room Air Temperature',
        outdoor_temp_col: str = 'Ambient Outdoor Temperature',
        power_col: str = "Total Unit Power",
        site_id: str = "S53",
        memory_length = 96,
        horizon_length = 4,
        train_block_size_in_days: int = 16,
        val_block_size_in_days: int = 2,
        test_block_size_in_days: int = 2,
        single_block_size: int = 96,  # 1 day in 15 min
):
    before = DateUtils.now()
    nyc_data_df = pd.read_csv(absolute_df_path_str,
                              index_col = datetime_col,
                              parse_dates = True)
    site_df = HeatPumpDatasetNYC.site_subset_df(nyc_data_df, site_id)
    site_df = site_df[[indoor_temp_col, outdoor_temp_col, power_col]]

    DataPreparationUtils.outlier_fix(site_df,
                                     indoor_temp_col,
                                     outdoor_temp_col,
                                     outlier_temp_threshold=15.)

    site_df = site_df.resample('15min').asfreq()
    site_df[indoor_temp_col] = site_df[indoor_temp_col].interpolate()
    site_df[outdoor_temp_col] = site_df[outdoor_temp_col].interpolate()

    # NOTE: visuals for debugging
    # VisualizationUtils.visualize_temperatures_and_power(
    #     site_df, power_col, outdoor_temp_col, indoor_temp_col
    # )
    # import matplotlib.pyplot as plt
    # plt.show()

    site_df = add_features_to_interval(site_df)

    train_dfs, val_dfs, test_dfs = partition_df(
        site_df=site_df,
        train_block_size_in_days=train_block_size_in_days,
        val_block_size_in_days=val_block_size_in_days,
        test_block_size_in_days=test_block_size_in_days,
        single_block_size=single_block_size
    )

    part_lam = lambda part_df: generate_xy_from_partition(partition=part_df,
                                                      power_col=power_col,
                                                      memory_length=memory_length,
                                                      horizon_length=horizon_length)

    after_xy = DateUtils.now()
    print(f"Generating full data took: {after_xy - before}")

    import itertools
    train_xys = list(itertools.chain(*list(map(part_lam, train_dfs))))
    val_xys = list(itertools.chain(*list(map(part_lam, val_dfs))))
    test_xys = list(itertools.chain(*list(map(part_lam, test_dfs))))

    after_flat = DateUtils.now()
    print(f"Flattening took: { after_flat - after_xy }")

    def check_if_df_has_no_nans(df: pd.DataFrame):
        return not df.isnull().values.any()

    train_xys_filtered = list(filter(lambda df: check_if_df_has_no_nans(df), train_xys))
    val_xys_filtered = list(filter(lambda df: check_if_df_has_no_nans(df), val_xys))
    test_xys_filtered = list(filter(lambda df: check_if_df_has_no_nans(df), test_xys))

    after_filtering = DateUtils.now()
    print(f"Filtering took: { after_filtering - after_flat }")


    # Filter out bad data
    # proper_splits = list(filter(lambda split: not split.isnull().values.any(), splits))

    return train_xys_filtered, val_xys_filtered, test_xys_filtered

class FullDataModule(pl.LightningDataModule):
    '''
    PyTorch Lighting DataModule subclass:
    https://pytorch-lightning.readthedocs.io/en/latest/datamodules.html

    Serves the purpose of aggregating all data loading
      and processing work in one place.
    '''

    # TODO: remove seq_len
    def __init__(self,
                 dataset_path: str,
                 batch_size: int,
                 seq_len: Optional[int],
                 site_ids: [str],
                 datetime_col: str,
                 power_col: str,
                 indoor_temp_col: str,
                 outdoor_temp_col: str,
                 idle_power_threshold: float,
                 horizon_length: int,
                 memory_length: int,
                 dataset_alias: str = "NYC_full",
                 # train_test_split_count: int = 8,
                 # train_val_split_count: int = 8,
                 add_time_features: bool = False,
                 flatten_xs: bool = False,
                 override_memory_length: Optional[int] = None,
                 remove_weekends_from_train_and_val: bool = False,
                 ):
        super().__init__()
        self.dataset_path = dataset_path
        self.seq_len = seq_len
        self.site_ids = site_ids
        self.datetime_col = datetime_col
        self.power_col = power_col
        self.indoor_temp_col = indoor_temp_col
        self.outdoor_temp_col = outdoor_temp_col
        self.idle_power_threshold = idle_power_threshold
        self.horizon_length = horizon_length
        self.memory_length = memory_length
        self.dataset_alias = dataset_alias
        self.interval_idx_col = "interval_idx"

        self.batch_size = batch_size
        self.num_workers = 0 # TODO: consider making this more???
        self.X_train = None
        self.y_train = None
        self.X_val = None
        self.y_val = None
        self.X_test = None
        self.X_test = None
        self.columns = None
        self.preprocessing = None
        self.scaler = None

        self.add_time_features = add_time_features
        self.feature_count = 9 + self.horizon_length
        self.flatten_xs = flatten_xs

        self.train_xys_filtered = None
        self.val_xys_filtered = None
        self.test_xys_filtered = None

        self.override_memory_length = override_memory_length
        self.remove_weekends_from_train_and_val = remove_weekends_from_train_and_val

        if (not add_time_features):
            raise NotImplementedError("Not adding time features is not supported!")

    def prepare_data(self):
        pass

    def setup(self, stage=None):
        # NOTE: stage == 'fit' -> train/val datasets
        #       stage == 'test' -> test datasets

        # Check if setup required, if not -> skip
        if stage == 'fit' and self.X_train is not None:
            return
        if stage == 'test' and self.X_test is not None:
            return
        if stage is None and self.X_train is not None and self.X_test is not None:
            return

        proper_splits: [pd.DataFrame]

        no_weekend_suffix = "" if not self.remove_weekends_from_train_and_val else "no_weekend"
        train_alias = self.dataset_alias + "_train" + no_weekend_suffix
        val_alias = self.dataset_alias + "_val" + no_weekend_suffix
        test_alias = self.dataset_alias + "_test" + no_weekend_suffix

        if (self.override_memory_length is not None):
            print(f"Using overriden memory length {self.override_memory_length}")
        memory_to_use = self.memory_length if self.override_memory_length is None else self.override_memory_length

        train_cached_file_path_obj: Path = Path(_cached_interval_file_path(dataset_alias=train_alias,
                                                                           site_ids=self.site_ids,
                                                                           memory=memory_to_use,
                                                                           horizon=self.horizon_length,
                                                                           features=self.feature_count))
        val_cached_file_path_obj = Path(_cached_interval_file_path(dataset_alias=val_alias,
                                                                        site_ids=self.site_ids,
                                                                        memory=memory_to_use,
                                                                        horizon=self.horizon_length,
                                                                        features=self.feature_count))
        test_cached_file_path_obj = Path(_cached_interval_file_path(dataset_alias=test_alias,
                                                                        site_ids=self.site_ids,
                                                                        memory=memory_to_use,
                                                                        horizon=self.horizon_length,
                                                                        features=self.feature_count))

        if ( train_cached_file_path_obj.exists() and
             val_cached_file_path_obj.exists() and
             test_cached_file_path_obj.exists() ):
            print(f"Found cached files in folder {train_cached_file_path_obj.parent.absolute()}")
            train_xys_filtered = load_intervals_from_csv(dataset_alias=train_alias,
                                    datetime_col=self.datetime_col,
                                    interval_idx_col=self.interval_idx_col,
                                    site_ids=self.site_ids,
                                    memory=memory_to_use,
                                    horizon=self.horizon_length,
                                    feature_count=self.feature_count)
            val_xys_filtered = load_intervals_from_csv(dataset_alias=val_alias,
                                                datetime_col=self.datetime_col,
                                                interval_idx_col=self.interval_idx_col,
                                                site_ids=self.site_ids,
                                                memory=memory_to_use,
                                                horizon=self.horizon_length,
                                                feature_count=self.feature_count)
            test_xys_filtered = load_intervals_from_csv(dataset_alias=test_alias,
                                                datetime_col=self.datetime_col,
                                                interval_idx_col=self.interval_idx_col,
                                                site_ids=self.site_ids,
                                                memory=memory_to_use,
                                                horizon=self.horizon_length,
                                                feature_count=self.feature_count)
        else:
            # train_array, val_array, test_array = np.array(train_list), np.array(val_list), np.array(test_list)
            train_xys_filtered, val_xys_filtered, test_xys_filtered = \
                prepare_full_data(
                    absolute_df_path_str=self.dataset_path,
                    datetime_col=self.datetime_col,
                    indoor_temp_col=self.indoor_temp_col,
                    outdoor_temp_col=self.outdoor_temp_col,
                    power_col=self.power_col,
                    site_id=self.site_ids[0], # TODO: assuming ONE site id
                    horizon_length=self.horizon_length,
                    memory_length=memory_to_use,
                )

            save_intervals_to_df(proper_intervals=train_xys_filtered,
                                 dataset_alias = train_alias,
                                 datetime_col = self.datetime_col,
                                 interval_idx_col = self.interval_idx_col,
                                 site_ids = self.site_ids,
                                 memory = memory_to_use,
                                 horizon = self.horizon_length,
                                 feature_count = self.feature_count)
            save_intervals_to_df(proper_intervals=val_xys_filtered,
                                 dataset_alias=val_alias,
                                 datetime_col=self.datetime_col,
                                 interval_idx_col=self.interval_idx_col,
                                 site_ids=self.site_ids,
                                 memory=memory_to_use,
                                 horizon=self.horizon_length,
                                 feature_count=self.feature_count)
            save_intervals_to_df(proper_intervals=test_xys_filtered,
                                 dataset_alias=test_alias,
                                 datetime_col=self.datetime_col,
                                 interval_idx_col=self.interval_idx_col,
                                 site_ids=self.site_ids,
                                 memory=memory_to_use,
                                 horizon=self.horizon_length,
                                 feature_count=self.feature_count)



        def grab_X_from_interval(interval_arr: np.array, y_length: int = self.horizon_length) -> np.array:
            return interval_arr[:-y_length]
        def grab_y_from_interval(interval_arr: np.array, y_length: int = self.horizon_length) -> np.array:
            return interval_arr[-y_length:]

        after_filtering = DateUtils.now()

        self.train_xys_filtered = train_xys_filtered
        self.val_xys_filtered = val_xys_filtered
        self.test_xys_filtered = test_xys_filtered

        ### Code for removing weekend records experiment!

        def remove_weekend_records(xys_filtered: [pd.DataFrame]):
            return list(filter(lambda xys: xys.iloc[0]['Weekday'] < 5 and xys.iloc[-1]['Weekday'] < 5, xys_filtered))

        if (self.remove_weekends_from_train_and_val):
            print("<<<<<< USING NO WEEKEND TRAIN AND VAL SETS >>>>>>")
            self.orig_train_xys_filtered = self.train_xys_filtered
            self.orig_val_xys_filtered = self.val_xys_filtered
            self.orig_test_xys_filtered = self.test_xys_filtered

            self.train_xys_filtered = remove_weekend_records(self.train_xys_filtered)
            self.val_xys_filtered = remove_weekend_records(self.val_xys_filtered)
            self.test_xys_filtered = remove_weekend_records(self.test_xys_filtered)

            print(f"\nFiltering weekends took: {DateUtils.now() - after_filtering}")
            print(f"Filtered {len(self.orig_train_xys_filtered) - len(self.train_xys_filtered)}/{len(self.orig_train_xys_filtered)} train items")
            print(f"Filtered {len(self.orig_val_xys_filtered) - len(self.val_xys_filtered)}/{len(self.orig_val_xys_filtered)} val items")
            print(f"Filtered {len(self.orig_test_xys_filtered) - len(self.test_xys_filtered)}/{len(self.orig_test_xys_filtered)} test items")

        train_array = np.stack(self.train_xys_filtered)
        val_array = np.stack(self.val_xys_filtered)
        test_array = np.stack(self.test_xys_filtered)

        if (self.override_memory_length is not None):
            print(f"Using memory length override! Previous test shape: {test_array.shape}")
            train_array = train_array[:,-(self.memory_length + self.horizon_length):,:]
            val_array = val_array[:,-(self.memory_length + self.horizon_length):,:]
            test_array = test_array[:,-(self.memory_length + self.horizon_length):,:]
            print(f"Current shape: {test_array.shape}")

        after_stack = DateUtils.now()
        print(f"Stacking took: {after_stack - after_filtering}")

        self.X_train = np.array(list(map(lambda interval: grab_X_from_interval(interval), train_array)))
        self.X_val = np.array(list(map(lambda interval: grab_X_from_interval(interval), val_array)))
        self.X_test = np.array(list(map(lambda interval: grab_X_from_interval(interval), test_array)))

        self.y_train = np.array(list(map(lambda interval: grab_y_from_interval(interval), train_array)))
        self.y_val = np.array(list(map(lambda interval: grab_y_from_interval(interval), val_array)))
        self.y_test = np.array(list(map(lambda interval: grab_y_from_interval(interval), test_array)))

        def flatten_out(X: np.array) -> np.array:
            static_values = X[0, 3:] # Caching only ONE sample of the static values
            ts_values = X[:, 0:3].reshape(-1) #Flattening out the values
            return np.append(ts_values, static_values)

        if (self.flatten_xs):
            self.X_train = np.array(list(map(lambda x: flatten_out(x), self.X_train)))
            self.X_val = np.array(list(map(lambda x: flatten_out(x), self.X_val)))
            self.X_test = np.array(list(map(lambda x: flatten_out(x), self.X_test)))

        self.scaler = StandardScaler() # TODO: consider alternative scalers, as well
        self.scaler.fit(np.vstack(self.X_train)) # NOTE: vstack makes one giant array along the 0th dimension (rows)

        if stage == 'fit' or stage is None:
            self.prepare_output() # We allow for overriding
            DataPreparationUtils.fail_if_nan(self.X_train)
            DataPreparationUtils.fail_if_nan(self.X_val)
            DataPreparationUtils.fail_if_nan(self.y_train)
            DataPreparationUtils.fail_if_nan(self.y_val)
            DataPreparationUtils.fail_if_nan(self.X_test)
            DataPreparationUtils.fail_if_nan(self.y_test)
            print("Setup() done")

    def _select_future_temps_from_y(self, y_arr: np.array) -> np.array:
        # TODO: 0 depends on the order of the dataframe, consider extracting this from dataframe
        return y_arr[:, 0]

    def prepare_output(self):
        # Scale, project, and reshape into right dimensions

        # Train
        self.X_train = np.array_split(self.scaler.transform(np.vstack(self.X_train)), len(self.X_train))
        self.y_train = np.array([self._select_future_temps_from_y(y) for y in self.y_train])
        self.y_train = self.y_train.reshape((-1, self.horizon_length))

        # Val
        self.X_val = np.array_split(self.scaler.transform(np.vstack(self.X_val)), len(self.X_val))
        self.y_val = np.array([self._select_future_temps_from_y(y) for y in self.y_val])
        self.y_val = self.y_val.reshape((-1, self.horizon_length))

        # Test
        self.X_test = np.array_split(self.scaler.transform(np.vstack(self.X_test)), len(self.X_test))
        self.y_test = np.array([self._select_future_temps_from_y(y) for y in self.y_test])
        self.y_test = self.y_test.reshape((-1, self.horizon_length))

    def train_dataloader(self):
        train_dataset = CustomIntervalDataset(
            self.X_train, self.y_train, )
        train_loader = DataLoader(train_dataset,
                                  batch_size = self.batch_size,
                                  shuffle = False,
                                  num_workers = self.num_workers)

        return train_loader

    def val_dataloader(self):
        val_dataset = CustomIntervalDataset(
            self.X_val, self.y_val, )
        val_loader = DataLoader(val_dataset,
                                batch_size = self.batch_size,
                                shuffle = False,
                                num_workers = self.num_workers)

        return val_loader

    def test_dataloader(self):
        test_dataset = CustomIntervalDataset(
            self.X_test, self.y_test,)
        test_loader = DataLoader(test_dataset,
                                 batch_size = self.batch_size,
                                 shuffle = False,
                                 num_workers = self.num_workers)

        return test_loader

class FlexibilityDataModule(FullDataModule):

    def __init__(self,
                 dataset_path: str,
                 batch_size: int,
                 seq_len: Optional[int],
                 site_ids: [str],
                 datetime_col: str,
                 power_col: str,
                 indoor_temp_col: str,
                 outdoor_temp_col: str,
                 idle_power_threshold: float,
                 horizon_length: int,
                 memory_length: int,
                 temperature_bound: float,
                 dataset_alias: str = "FLEX_full",
                 add_time_features: bool = False,
                 flatten_xs: bool = False,
                 override_memory_length: Optional[int] = None,
                 remove_weekends_from_train_and_val: bool = False,
                 ):
        super().__init__(
            dataset_path,
            batch_size,
            seq_len,
            site_ids,
            datetime_col,
            power_col,
            indoor_temp_col,
            outdoor_temp_col,
            idle_power_threshold,
            horizon_length,
            memory_length,
            dataset_alias,
            add_time_features,
            flatten_xs,
            override_memory_length,
            remove_weekends_from_train_and_val,
        )
        self.temperature_bound = temperature_bound

    def flex_alg(self,
                abs_diff_array: np.ndarray,
                temperature_bound: float):
        flex = 0
        for sub in abs_diff_array:
            if (sub <= temperature_bound):
                flex += 1
            else:
                break
        return flex

    def extract_flexibility(self,
                            last_temps: np.ndarray,
                            y_vals: np.ndarray):
        if (not last_temps.shape[0] == y_vals.shape[0]):
            raise ValueError("Number of records in last temperatures and y vals are different!")
        subs = np.subtract(y_vals, last_temps.reshape([-1, 1]))
        y_flexibility = np.apply_along_axis(lambda sub: self.flex_alg(sub, self.temperature_bound), 1, subs).reshape([-1, 1])
        return y_flexibility

    def prepare_output(self):
        # TODO: remove 'self' here
        self.last_temps_train = self.X_train[:, -1, 0]
        self.last_temps_val = self.X_val[:, -1, 0]
        self.last_temps_test = self.X_test[:, -1, 0]

        super().prepare_output()

        self.y_train = self.extract_flexibility(self.last_temps_train, self.y_train)
        self.y_val = self.extract_flexibility(self.last_temps_val, self.y_val)
        self.y_test = self.extract_flexibility(self.last_temps_test, self.y_test)

        # TODO: take the last temperature of X
        # Do preprocessing "normally"
        # absolute subtract the last temperature
        # count from start how many points are within threshold
        # set this to new y_test
        # Make this into method, takes X and y
        print("TODO: impl prepare_output")
        pass

        # super().prepare_output()
        # Train
        # self.X_train = np.array_split(self.scaler.transform(np.vstack(self.X_train)), len(self.X_train))
        # self.y_train = np.array([self._select_future_temps_from_y(y) for y in self.y_train])
        # self.y_train = self.y_train.reshape((-1, self.horizon_length))
        #
        # # Val
        # self.X_val = np.array_split(self.scaler.transform(np.vstack(self.X_val)), len(self.X_val))
        # self.y_val = np.array([self._select_future_temps_from_y(y) for y in self.y_val])
        # self.y_val = self.y_val.reshape((-1, self.horizon_length))
        #
        # # Test
        # self.X_test = np.array_split(self.scaler.transform(np.vstack(self.X_test)), len(self.X_test))
        # self.y_test = np.array([self._select_future_temps_from_y(y) for y in self.y_test])
        # self.y_test = self.y_test.reshape((-1, self.horizon_length))

# NOTE: for testing
def get_test_dm():
    fdm = FlexibilityDataModule(
        dataset_path="./output/NYC_data_15min.csv",
        datetime_col= "DateTime",
        indoor_temp_col = 'Room Air Temperature',
        outdoor_temp_col = 'Ambient Outdoor Temperature',
        power_col = "Total Unit Power",
        site_ids=["S53"],
        memory_length = 16,
        horizon_length = 4,
        batch_size=1,
        seq_len=1,
        idle_power_threshold=100,
        add_time_features=True,
        temperature_bound=0.5, # Exclusive to FlexibilityDataModule
    )
    fdm.setup(stage='fit')
    return fdm

if __name__ == '__main__':
    dm = get_test_dm()

    last_temps = dm.last_temps_test
    y_vals = dm.y_test

    # df_0 = dm.train_xys_filtered[0]
    # X_array_0 = dm.X_train[0,:,:]
    # y_array_0 = dm.y_train[0,:,:]
    #
    # X_array_3 = dm.X_train[3,:,:]
    # y_array_3 = dm.y_train[3,:,:]
    #
    # last_temps = dm.X_train[:,-1,0]
