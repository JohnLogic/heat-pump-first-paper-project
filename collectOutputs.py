import argparse
import re

import pytorch_lightning.metrics.functional as plmf
import torch

import experimentCommons
import torchMetrics
from utils import *

try:

    LONG_HORIZON_IN_HOURS = 24 # Horizon of long prediction in 15 min intervals

    POWER_OFF_THRESHOLDS = {
        'neogrid2058': 41.,
        'neogrid2246': 0.6,
        'else': 100,
    }

    POWER_ON_QUANTILES = {
        'neogrid2246': 0.75,
        'else': 0.85,
    }

    if __name__ == '__main__':

        print("Script started!")

        AVAILABLE_MODES = ["base", "eval", "flexibility", "full", "long"]
        DEFAULT_MODE = "base"
        DEFAULT_BATCH_SIZE = 1

        ap = argparse.ArgumentParser(
            description="This script runs training of neural networks for Low-dose CT image reconstruction tasks")
        ap.add_argument("-o", "--outputdir", required=True,
                        help="experiment output folder (for storing models, logs, etc.)")
        ap.add_argument("-m", "--mode", required=False,
                        help=f"selects which mode to use (available modes: {str(AVAILABLE_MODES)} | base is default")
        ap.add_argument("--data_nyc", required=True)
        ap.add_argument("--data_nist", required=True)
        ap.add_argument("--data_neogrid", required=True) # NOTE: this is the one with central heating
        ap.add_argument("--data_neogrid_proper", required=True) # NOTE: this is the one with the actual HP (!)
        ap.add_argument("--override_outputs", required=False)
        ap.add_argument("--arima_test_dir", required=False,
                        help="This directory specifies if suplementary SARIMAX model tests should be appended")
        ap.add_argument("--override_memory_length", required=False,)
        ap.add_argument("-nw", "--no_weekend", required=False,
                        help=f"Removes weekend values from train and val sets")
        # ap.add_argument("-b", "--inference_batch_size", required=False,
        #                 help=f"batch size to be used during inference ({DEFAULT_BATCH_SIZE})")

        args = vars(ap.parse_args())

        output_dir = args['outputdir']
        nyc_data_dir = args['data_nyc']
        nist_data_dir = args['data_nist']
        neogrid_data_dir = args['data_neogrid']
        neogrid_proper_data_dir = args['data_neogrid_proper']

        override_outputs = bool(args['override_outputs'])

        arima_test_dir = args['arima_test_dir']
        override_memory_length = \
            int(args['override_memory_length']) if args['override_memory_length'] else None

        mode = args['mode'] if args['mode'] is not None else DEFAULT_MODE

        if not Path(output_dir).is_dir():
            raise ValueError(f"Given path {output_dir} is not a directory!")

        if not Path(nyc_data_dir).exists():
            raise ValueError(f"Given NYC data path {nyc_data_dir} does not exist!")

        if not Path(nist_data_dir).exists():
            raise ValueError(f"Given NIST data path {nist_data_dir} does not exist!")

        if not Path(neogrid_data_dir).exists():
            raise ValueError(f"Given NEOGRID data path {neogrid_data_dir} does not exist!")

        if neogrid_proper_data_dir is None or not Path(neogrid_proper_data_dir).exists():
            raise ValueError(f"Proper NEOGRID data path (for 2246) not given! Outputs will not be collected!")

        no_weekend = bool(args["no_weekend"])

        # inference_batch_size = int(args['inference_batch_size']) if args['inference_batch_size'] is not None \
        #     else DEFAULT_BATCH_SIZE
        inference_batch_size = 1

        # --- Collect all the result files ---
        experiment_result_folder = output_dir
        result_folder_path_list = []

        for experiment_folder in os.listdir(experiment_result_folder):
            if (experiment_folder.startswith('.')):
                continue
            experiment_folder_full_path = os.path.join(experiment_result_folder, experiment_folder)
            if not Path(experiment_folder_full_path).is_dir():
                continue
            folder_result_file_list = list(filter(lambda f: f.endswith('output_object.csv'), os.listdir(experiment_folder_full_path)))

            if (len(folder_result_file_list) != 0):
                file_path = os.path.join(experiment_folder_full_path, folder_result_file_list[0])
                result_folder_path_list.append((experiment_folder, file_path))

        if (len(result_folder_path_list) == 0):
            ValueError(f"No experiments found in folder: {experiment_result_folder}!")

        # --- Concat them to one single dataframe and output ---
        now_str = DateUtils.formatted_datetime_now()
        combined_result_df = pd.concat(list(map(lambda folder_path_tuple: pd.read_csv(folder_path_tuple[1]),
                                                result_folder_path_list)), ignore_index=True)
        combined_result_df.to_csv(os.path.join(experiment_result_folder,f"{now_str}.csv"), index=False, header=True)

        def get_parameters_from_output_df(output_df):
            param_dict = {}
            param_dict['date'] = output_df['date'].item()
            param_dict['experiment_name'] = output_df['experiment_name'].item()
            param_dict['model_name'] = str(output_df['model'].item()) # TODO: need parsing
            param_dict['layers'] = int(output_df['layers']) # TODO: need parsing
            param_dict['hidden_size'] = int(output_df['hidden_size'])
            param_dict['memory_length'] = int(output_df['memory_length'])
            param_dict['horizon_length'] = int(output_df['horizon_length']) # TODO: implement more than 4
            param_dict['site_ids_string'] = str(output_df['site_ids'].item()) # important - says what dataset it is
            param_dict['time_features'] = output_df['time_features'].bool()
            param_dict['seq_len'] = 1
            param_dict['batch_size'] = int(output_df['batch_size'])
            param_dict['num_layers'] = int(output_df['layers'])
            param_dict['n_features'] = 13 if (output_df["dataloader_type"].item() == "full") \
                else 9 if (param_dict['time_features']) else 3
            param_dict['dropout'] = 0.1 #TODO HARDCODED!
            param_dict['learning_rate'] = 0.005 #TODO HARDCODED!
            param_dict['dataloader_type'] = str(output_df['dataloader_type'].item())
            param_dict['input_type'] = experimentCommons.get_appropriate_input_type(param_dict['model_name'])

            param_dict['site_ids'] = experimentCommons.get_site_ids_from_string(param_dict['site_ids_string'])
            if (len(param_dict['site_ids']) > 1):
                raise ValueError(f"More than 1 site id is unsupported! Given: {param_dict['site_ids']}")

            param_dict['single_site_id'] = param_dict['site_ids'][0]
            single_site_id = param_dict['single_site_id']

            param_dict['train_loss'] = float(output_df['train_loss'])
            param_dict['val_loss'] = float(output_df['val_loss'])
            param_dict['test_loss'] = float(output_df['test_loss'])

            def select_data_dir(site_id: str):
                neogrid_pattern = 'neogrid2058'
                neogrid_proper_pattern = 'neogrid2246'
                nist_pattern = 'nist'
                nyc_pattern = '^S\d\d$'
                if (bool(re.search(pattern=nyc_pattern, string=site_id))):
                    return nyc_data_dir
                elif (neogrid_proper_pattern in site_id):
                    return neogrid_proper_data_dir
                elif (neogrid_pattern in site_id):
                    return neogrid_data_dir
                elif (nist_pattern in site_id):
                    return nist_data_dir
                else:
                    raise ValueError(f"Site id ({site_id}) does not have an associated data_dir path!")

            # NOTE: else case is lenient because select_data_dir catches 'random' site ids already
            param_dict['power_threshold'] = POWER_OFF_THRESHOLDS[single_site_id] \
                if single_site_id in POWER_OFF_THRESHOLDS \
                else POWER_OFF_THRESHOLDS['else']

            param_dict['data_dir'] = select_data_dir(single_site_id)

            loss_map = experimentCommons.select_loss_map(param_dict)
            loss_from_params = str(output_df['loss_function'].item())
            loss = loss_map.get(loss_from_params)
            if (loss is None):
                str_list_of_available_losses = ", ".join(list(loss_map.keys()))
                raise ValueError(
                    'Loss with name "' + loss_from_params  + '" is not defined!\nList of available loss functions: ' + str_list_of_available_losses)
            param_dict['loss'] = loss
            param_dict['add_time_features'] = param_dict['time_features']

            return param_dict

        param_list = []
        if (mode in ["eval", "flexibility", "full", "long"]):

            # We decompose param loop from eval loop for safety
            for folder_path, output_df_path in result_folder_path_list:
                try:
                    full_folder_path = os.path.join(experiment_result_folder, folder_path)
                    output_df = pd.read_csv(output_df_path)

                    chkpoint_folder_path = os.path.join(full_folder_path, "checkpoints", "best")
                    if not Path(chkpoint_folder_path).is_dir():
                        print(f"Checkpoint folder not found! {chkpoint_folder_path}")
                        continue # TODO: report somehow

                    chkpoint_relative_path = next(filter(lambda f: f.endswith('.ckpt'), os.listdir(chkpoint_folder_path)),
                                         None) # Here None is the default value
                    if chkpoint_relative_path is None:
                        print(f"Checkpoint file not found!")
                        continue # TODO: report somehow

                    param_dict = get_parameters_from_output_df(output_df)
                    param_dict['full_folder_path'] = full_folder_path

                    chkpoint_path = os.path.join(chkpoint_folder_path, chkpoint_relative_path)
                    param_dict['checkpoint_path'] = chkpoint_path

                    output_result_path = os.path.join(full_folder_path, "full_output.csv")
                    param_dict['output_result_path'] = output_result_path

                    long_output_result_path = os.path.join(full_folder_path, "long_output.csv")
                    param_dict['long_output_result_path'] = long_output_result_path

                    param_list.append(param_dict)
                except Exception as e:
                    pd.DataFrame({'error': str(e)}, index=[0]).to_csv(os.path.join(full_folder_path, "param_dict_error.csv"))
                    print(f"Error occured during param_dict construction : {e}")

            if (mode != "long"):
                ### >>>> NORMAL PATH, FULL INFERENCE <<<<
                for param_dict in param_list:
                    try:
                        full_folder_path = param_dict['full_folder_path']
                        chekpoint_path = param_dict['checkpoint_path']
                        output_result_path = param_dict['output_result_path']

                        if (not override_outputs and Path(output_result_path).exists()):
                            print(f"'{output_result_path}' already exists! Skipping...")
                            continue

                        model = experimentCommons.load_model_from_checkpoint(checkpoint_path=chekpoint_path, param_dict=param_dict)
                        model.freeze() # why do we need freeze()

                        test_dm = experimentCommons.get_data_module(
                            param_dict=param_dict,
                            type=param_dict['dataloader_type'],
                            override_batch_size=inference_batch_size, # Take bigger batch size for faster speed!
                            input_type=param_dict['input_type'],
                            no_weekend=no_weekend
                        )
                        if (override_memory_length is not None):
                            test_dm = experimentCommons.get_data_module(
                                param_dict=param_dict,
                                type=param_dict['dataloader_type'],
                                override_batch_size=inference_batch_size,  # Take bigger batch size for faster speed!
                                input_type=param_dict['input_type'],
                                override_memory_length=override_memory_length,
                                no_weekend=no_weekend
                            )
                        test_dm.setup('fit')
                        test_dl = test_dm.test_dataloader()

                        before_inference = DateUtils.now()

                        rmse_loss_foo = torchMetrics.RMSELoss()
                        max_loss_foo = torchMetrics.maximum_absolute_error()

                        horizon = param_dict['horizon_length']
                        result_tensor = torch.tensor([])

                        for X_batch, y_batch in test_dl:
                            transformed_X_batch = test_dm.scaler.inverse_transform(X_batch)
                            proper_X_batch = experimentCommons.get_proper_X(transformed_X_batch,
                                                                            memory_length=param_dict['memory_length'],
                                                                            input_type=param_dict['input_type'])
                            last_X = proper_X_batch[0, -1, :]
                            start_temperature = torch.tensor([last_X[0]])
                            power_for_horizon = last_X[-horizon:]
                            date_tensor = last_X[3:6]

                            y_batch_hat = (model(X_batch))
                            loss_tensor = torch.tensor([rmse_loss_foo(y_batch, y_batch_hat),
                                          plmf.mean_absolute_error(y_batch, y_batch_hat),
                                          max_loss_foo(y_batch, y_batch_hat)
                                          ]).reshape([1, -1])
                            result_tensor = \
                                torch.cat([result_tensor,
                                           torch.cat([
                                               torch.cat([y_batch, y_batch_hat, loss_tensor], dim=1), # move to start for easier access
                                               torch.cat([start_temperature, power_for_horizon, date_tensor]).reshape([1, -1]),
                                           ], dim=1)])

                        after_inference = DateUtils.now()
                        result_array = result_tensor.numpy()

                        ### ----------------- Building the dataframe --------------------------

                        print(f"Batch size used: {inference_batch_size}")
                        print(f"Inference took: {after_inference - before_inference}")

                        y_columns = [f'truth_{i}' for i in range(horizon)] + [f'pred_{i}' for i in range(horizon)]
                        loss_columns = ['rmse', 'mae', 'maxe']
                        start_temperature_column = ['start_temperature']
                        power_for_horizon_columns = [f'power_{i}' for i in range(horizon)]
                        day_month_year_columns = ['day', 'month', 'year']
                        columns = y_columns + loss_columns + start_temperature_column + power_for_horizon_columns + day_month_year_columns

                        if (len(columns) != result_tensor.shape[1]):
                            raise ValueError(f"Generated column count {len(columns)} does not correspond with result_tensor column count {result_tensor.shape[1]}!")

                        combined_dict = {}
                        for idx, column in enumerate(columns):
                            combined_dict[column] = result_tensor[:, idx]

                        result_df = pd.DataFrame(combined_dict)
                        # result_df = result_df.round({'year': 0, 'month': 0, 'day': 0})
                        # pd.to_datetime(result_df.year*10000+(result_df.month+1)*100+(result_df.day),format='%Y%m%d')
                        print(f"Saving to: '{output_result_path}'")
                        result_df.to_csv(
                            output_result_path,
                            index=False,
                            header=True
                        )

                        after_generation = DateUtils.now()
                        print(f"Dataframe generation took: {after_generation - after_inference}")
                    except Exception as e:
                        pd.DataFrame({'error': str(e)}, index=[0]).to_csv(os.path.join(full_folder_path, "infer_error.csv"))
                        print(f"Error occured during inference : {e}")
                        raise e
            else:

                for param_dict in param_list:
                    try:
                        full_folder_path = param_dict['full_folder_path']
                        chekpoint_path = param_dict['checkpoint_path']
                        long_output_result_path = param_dict['long_output_result_path']

                        if (not override_outputs and Path(long_output_result_path).exists()):
                            print(f"'{long_output_result_path}' already exists! Skipping...")
                            continue

                        model = experimentCommons.load_model_from_checkpoint(checkpoint_path=chekpoint_path,
                                                                             param_dict=param_dict)
                        model.freeze()  # why do we need freeze()

                        test_dm = experimentCommons.get_data_module(
                            param_dict=param_dict,
                            type=param_dict['dataloader_type'],
                            override_batch_size=inference_batch_size,  # Take bigger batch size for faster speed!
                            input_type=param_dict['input_type'],
                        )
                        if (override_memory_length is not None):
                            test_dm = experimentCommons.get_data_module(
                                param_dict=param_dict,
                                type=param_dict['dataloader_type'],
                                override_batch_size=inference_batch_size,  # Take bigger batch size for faster speed!
                                input_type=param_dict['input_type'],
                                override_memory_length=override_memory_length,
                            )
                        test_dm.setup('fit')
                        test_dl = test_dm.test_dataloader()

                        before_long_exp = DateUtils.now()

                        # rmse_loss_foo = torchMetrics.RMSELoss()
                        # max_loss_foo = torchMetrics.maximum_absolute_error()

                        horizon = param_dict['horizon_length']
                        result_tensor = torch.tensor([])

                        for X_batch, y_batch in test_dl:
                            transformed_X_batch = test_dm.scaler.inverse_transform(X_batch)
                            proper_X_batch = experimentCommons.get_proper_X(transformed_X_batch,
                                                                            memory_length=param_dict['memory_length'],
                                                                            input_type=param_dict['input_type'])


                            def toi(tensor: torch.tensor):
                                return int(tensor.item())

                            def gen_new_X(proper_X_batch, y_batch_hat):
                                # Generates new X using predictions y_hat
                                from datetime import datetime, timedelta

                                prefix_X = proper_X_batch[:, 4:, :]
                                last_of_prefix_X = torch.squeeze(proper_X_batch[:, -1, :])
                                last_outdoor_temp = last_of_prefix_X[1]
                                last_day = last_of_prefix_X[3]
                                last_month = last_of_prefix_X[4]
                                last_year = last_of_prefix_X[5]
                                last_hour = last_of_prefix_X[6]
                                last_minute = last_of_prefix_X[7]

                                try:
                                    date = datetime(year = toi(last_year),
                                                    month=toi(last_month) if toi(last_month) > 0 else 1,
                                                    day=toi(last_day),
                                                    hour=toi(last_hour),
                                                    minute=toi(last_minute),
                                                    second=0,
                                                    microsecond=0)
                                except BaseException as e:
                                    date = datetime(year=toi(last_year),
                                                    month=toi(last_month)+1,
                                                    day=toi(last_day),
                                                    hour=toi(last_hour),
                                                    minute=toi(last_minute),
                                                    second=0,
                                                    microsecond=0)

                                new_X_tensor_list = []
                                for new_temp_val in torch.squeeze(y_batch_hat):
                                    date: datetime = date + timedelta(minutes=15)
                                    new_temp_tensor = torch.tensor([new_temp_val, last_outdoor_temp, 0.0,
                                                  date.day, date.month, date.year,
                                                  date.hour, date.minute, date.weekday(),
                                                  0, 0, 0, 0]).reshape(1, -1)
                                    new_X_tensor_list.append(new_temp_tensor)

                                new_X_tensor = torch.cat(new_X_tensor_list)
                                result = torch.cat(
                                                    [prefix_X,
                                                     new_X_tensor.reshape([1, 4, -1])
                                                    ],
                                                   dim=1
                                )
                                return result


                            # Loop for generating long Y
                            proper_X = proper_X_batch
                            long_y_list = []
                            for time_ahead in range(0, LONG_HORIZON_IN_HOURS):

                                if (param_dict['input_type'] == 'flat'):
                                    # flatten the proper_X before transforming and feeding into the model
                                    def flatten_out(proper_X: torch.tensor) -> torch.tensor:
                                        squeezed_X = torch.squeeze(proper_X)
                                        static_values = squeezed_X[0, 3:]  # Caching only ONE sample of the static values
                                        ts_values = squeezed_X[:, 0:3].reshape(-1)  # Flattening out the values
                                        return torch.cat([ts_values, static_values])
                                    flat_X = flatten_out(proper_X)
                                    X = torch.unsqueeze(torch.tensor(test_dm.scaler.transform(torch.unsqueeze(flat_X, dim=0)), dtype=torch.float32), dim=0)
                                else:
                                    X = torch.unsqueeze(torch.tensor(test_dm.scaler.transform(torch.squeeze(proper_X)), dtype=torch.float32), dim=0)

                                y_hat = (model(X))
                                new_proper_X = gen_new_X(proper_X, y_hat)
                                proper_X = new_proper_X
                                long_y_list.append(y_hat)

                            long_y_tensor = torch.cat(long_y_list).reshape(-1)

                            last_X = proper_X_batch[0, -1, :]
                            start_temperature = torch.tensor([last_X[0]])
                            start_outdoor_temperature = torch.tensor([last_X[1]])
                            date_tensor = last_X[3:6]
                            time_tensor = last_X[6:8]

                            row_result = torch.cat([start_temperature, start_outdoor_temperature, date_tensor, time_tensor, long_y_tensor])
                            result_tensor = torch.cat([result_tensor, row_result])

                            # Generation of the result tensor
                            # Initial: Indoor and Outdoor temps
                            # Date
                            # Ys


                            # result_tensor = \
                            #     torch.cat([result_tensor,
                            #                torch.cat([
                            #                    torch.cat([y_batch, y_batch_hat, loss_tensor], dim=1), # move to start for easier access
                            #                    torch.cat([start_temperature, power_for_horizon, date_tensor]).reshape([1, -1]),
                            #                ], dim=1)])


                        start_temperature_column = ['start_temperature']
                        outdoor_temperature_column = ['outdoor_temperature']
                        day_month_year_columns = ['day', 'month', 'year']
                        hour_minute_columns = ['hour', 'minute']
                        y_columns = [f'pred_{i}' for i in range(LONG_HORIZON_IN_HOURS * 4)]
                        columns = start_temperature_column + outdoor_temperature_column + day_month_year_columns + hour_minute_columns + y_columns

                        # result_tensor = torch.stack(result_list)
                        result_tensor = result_tensor.reshape([-1, len(columns)])
                        if (len(columns) != result_tensor.shape[1]):
                            raise ValueError(f"Generated column count {len(columns)} does not correspond with result_tensor column count {result_tensor.shape[1]}!")

                        combined_dict = {}
                        for idx, column in enumerate(columns):
                            combined_dict[column] = result_tensor[:, idx]

                        result_df = pd.DataFrame(combined_dict)
                        # result_df = result_df.round({'year': 0, 'month': 0, 'day': 0})
                        # pd.to_datetime(result_df.year*10000+(result_df.month+1)*100+(result_df.day),format='%Y%m%d')
                        print(f"Saving to: '{long_output_result_path}'")
                        result_df.to_csv(
                            long_output_result_path,
                            index=False,
                            header=True
                        )

                        after_long_exp = DateUtils.now()
                        result_array = result_tensor.numpy()

                        print(f"Batch size used: {inference_batch_size}")
                        print(f"Long experiment took: {after_long_exp - before_long_exp}")
                    except Exception as e:
                        raise e
                    finally:
                        print("Long experiments done!")


        low_dfs = {
            'S44': pd.DataFrame({}),
            'nist2013': pd.DataFrame({}),
            'neogrid2246': pd.DataFrame({}),
        }

        med_dfs = {
            'S44': pd.DataFrame({}),
            'nist2013': pd.DataFrame({}),
            'neogrid2246': pd.DataFrame({}),
        }

        high_dfs = {
            'S44': pd.DataFrame({}),
            'nist2013': pd.DataFrame({}),
            'neogrid2246': pd.DataFrame({}),
        }

        if (mode in ['long']):
            for param_dict in param_list:
                try:
                    full_folder_path = param_dict['full_folder_path']
                    chekpoint_path = param_dict['checkpoint_path']
                    output_result_path = param_dict['output_result_path']
                    long_output_result_path = param_dict['long_output_result_path']

                    # output_df = pd.read_csv(output_result_path)
                    long_output_df = pd.read_csv(long_output_result_path)

                    # -----

                    def get_datetime_from_df(df, year, month, day, hour, minute):
                        filter = (df['year'].round() == year) &\
                                 (df['month'].round() == month) &\
                                 (df['day'].round() == day) &\
                                 (df['hour'].round() == hour) &\
                                 (df['minute'].round() == minute)
                        if (len(df[filter]) == 0):
                            raise ValueError(f"Dataframe with filter returns empty result! \nParam dict: {param_dict}")

                        result_df = df[filter].copy()
                        result_df.insert(0, 'experiment_name', param_dict['experiment_name'])
                        result_df.insert(1, 'site_id', param_dict['single_site_id'])
                        result_df.insert(2, 'model_name', param_dict['model_name'])
                        result_df.insert(3, 'hidden_size', param_dict['hidden_size'])
                        result_df.insert(4, 'memory_length', param_dict['memory_length'])
                        return result_df

                    single_site_id = param_dict['single_site_id']
                    if (single_site_id == 'S44'):
                        high = get_datetime_from_df(long_output_df, 2016, 7, 2, 14, 45)
                        med = get_datetime_from_df(long_output_df, 2016, 6, 12, 19, 0)
                        low = get_datetime_from_df(long_output_df, 2016, 2, 14, 4, 30)
                    elif (single_site_id == 'neogrid2246'):
                        high = get_datetime_from_df(long_output_df, 2020, 8, 7, 19, 0)
                        med = get_datetime_from_df(long_output_df, 2020, 9, 16, 17, 30)
                        low = get_datetime_from_df(long_output_df, 2020, 12, 25, 20, 0)
                    elif (single_site_id == 'nist2013'):
                        high = get_datetime_from_df(long_output_df, 2013, 8, 9, 12, 0)
                        med = get_datetime_from_df(long_output_df, 2014, 4, 26, 4, 30)
                        low = get_datetime_from_df(long_output_df, 2014, 3, 17, 7, 15)
                    else:
                        raise ValueError(f"{single_site_id} not handled in long OFF experiment aggregation!")

                    high_dfs.update({single_site_id: high_dfs[single_site_id].append(high, ignore_index=True)})
                    med_dfs.update({single_site_id: med_dfs[single_site_id].append(med, ignore_index=True)})
                    low_dfs.update({single_site_id: low_dfs[single_site_id].append(low, ignore_index=True)})

                except BaseException as e:
                    print(e)
                    raise e

            for site in ['S44', 'nist2013', 'neogrid2246']:
                high_dfs[site].to_csv(os.path.join(experiment_result_folder,f"long_high_{site}.csv"), index=False, header=True)
                med_dfs[site].to_csv(os.path.join(experiment_result_folder,f"long_med_{site}.csv"), index=False, header=True)
                low_dfs[site].to_csv(os.path.join(experiment_result_folder,f"long_low_{site}.csv"), index=False, header=True)

        # -------------------------------------------------------------------

        append_extra_outputs: bool = False
        if (arima_test_dir is not None):
            if not Path(arima_test_dir).is_dir():
                raise ValueError("Given ARIMA test path is not a directory!")
            append_extra_outputs = True

        if (append_extra_outputs):
            # Create param_dicts with appropriate settings
            # Use the file name to differentiate the memory type and the dataset
            # Everything else should stay the same
            # Instead of val_loss, use test loss <- unless there is a better alternative (TODO: see if there is train loss available)

            extra_result_files = list(filter(lambda file:
                                             # Don't shoot yourself in the foot, folders have the same dir
                                             file.startswith("arima_results") and not Path(file).is_dir(),
                                             os.listdir(arima_test_dir)))

            for result_file in extra_result_files:
                # Assumes usage of the same convention
                mem = result_file.split('_')[-1].split('.')[-2]
                site_id = result_file.split('_')[-2]
                full_result_file_path = os.path.join(arima_test_dir, result_file)

                result_file_df = pd.read_csv(full_result_file_path)
                test_rmse_mean = result_file_df['rmse'].mean()

                p = {}
                p['horizon_length'] = 4
                p['memory_length'] = mem
                p['site_ids'] = [site_id] # "Get dynamically from file name"
                p['add_time_features'] = False
                p['input_type'] = 'flat'
                p['dataloader_type'] = 'full'
                p['batch_size'] = 1
                p['seq_len'] = 1
                p['date'] = DateUtils.now()
                p['model_name'] = "sarimax"
                p['layers'] = 0
                p['hidden_size'] = 0
                p['n_features'] = 13 # ???
                p['dropout'] = 0.0
                p['learning_rate'] = 0.0
                p['single_site_id'] = site_id
                p['val_loss'] = test_rmse_mean # TODO: calculate
                p['loss'] = 'RMSE'

                selected_site_id = p['site_ids'][0]
                if (selected_site_id == 'S44'):
                    p['power_threshold'] = POWER_OFF_THRESHOLDS['else']
                    p['data_dir'] = nyc_data_dir
                elif (selected_site_id == 'neogrid2058'):
                    p['power_threshold'] = POWER_OFF_THRESHOLDS[selected_site_id]
                    p['data_dir'] = neogrid_data_dir
                elif (selected_site_id == 'neogrid2246'):
                    p['power_threshold'] = POWER_OFF_THRESHOLDS[selected_site_id]
                    p['data_dir'] = neogrid_proper_data_dir
                elif (selected_site_id == 'nist2013'):
                    p['power_threshold'] = POWER_OFF_THRESHOLDS['else']
                    p['data_dir'] = nist_data_dir
                elif ('S' in selected_site_id):
                    p['power_threshold'] = POWER_OFF_THRESHOLDS['else']
                    p['data_dir'] = nyc_data_dir
                else:
                    raise ValueError(f'{selected_site_id} is not valid!')

                folder_directory = result_file.split('.')[-2]
                folder_directory = os.path.join(arima_test_dir, folder_directory)
                FileUtils.create_dir(folder_directory)

                p['full_folder_path'] = folder_directory
                p['output_result_path'] = os.path.join(arima_test_dir, result_file)

                p['checkpoint_path'] = ''

                param_list.append(p)

        if (mode in ["flexibility", "full"]):
            print("\n--- Flexibility extraction ---\n")

            for param_dict in param_list:
                try:
                    result_df = pd.read_csv(param_dict['output_result_path'])

                    full_folder_path = param_dict['full_folder_path']
                    flex_on_output_path = os.path.join(full_folder_path, "flex_on_output.csv")
                    flex_off_output_path = os.path.join(full_folder_path, "flex_off_output.csv")

                    horizon = param_dict['horizon_length']
                    rmse_error_for_adjustment = param_dict['val_loss']

                    # Create filter for three scenarios "full | off | mixed"

                    # TODO: these thresholds are questionable, figure something out
                    force_on_threshold = result_df['power_0'].quantile(0.85)
                    print(f"SITE: {param_dict['single_site_id']} | ON threshold: {force_on_threshold}")
                    force_off_threshold = param_dict['power_threshold']

                    force_on_filter_foo = lambda idx: result_df[f'power_{idx}'] >= force_on_threshold
                    force_off_filter_foo = lambda idx: result_df[f'power_{idx}'] <= force_off_threshold
                    mixed_filter_foo = lambda idx: (force_on_filter_foo(idx) | force_off_filter_foo(idx))

                    force_on_filter_3 = force_on_filter_foo(0)
                    force_off_filter_3 = force_off_filter_foo(0)
                    mixed_filter_4 = mixed_filter_foo(0)

                    for idx in range(1, horizon-1):
                        force_on_filter_3 = force_on_filter_3 & force_on_filter_foo(idx)
                        force_off_filter_3 = force_off_filter_3 & force_off_filter_foo(idx)

                    for idx in range(1, horizon):
                        mixed_filter_4 = mixed_filter_4 & mixed_filter_foo(idx)

                    # Diverging ON
                    force_on_filter_4 = force_on_filter_3 & force_on_filter_foo(horizon-1)
                    force_on_filter_3 = force_on_filter_3 & ~force_on_filter_foo(horizon-1)

                    force_off_filter_4 = force_off_filter_3 & force_off_filter_foo(horizon-1)
                    force_off_filter_3 = force_off_filter_3 & ~force_off_filter_foo(horizon-1)

                    # Calculate metrics for scenarios
                    force_on_df = pd.concat([
                        result_df[force_on_filter_3],
                        result_df[force_on_filter_4],
                    ])

                    force_off_df = pd.concat([
                        result_df[force_off_filter_3],
                        result_df[force_off_filter_4],
                    ])

                    mixed_df = result_df[mixed_filter_4]

                    # Extracting flexibility from starting temperature and given bounds
                    def extract_flex(starting_temp: float,
                                     temps: np.array,
                                     is_sequential: bool,  # true - extracts sequentially from start
                                     duration: int, # length of operation
                                     error_adjust: float = 0.,
                                     fixed_bounds: Optional = None,
                                     dynamic_bounds: Optional[float] = None):
                        if (fixed_bounds and dynamic_bounds):
                            raise ValueError("fixed_bounds and dynamic_bounds are mutually exclusive! Choose one :)")
                        if (not fixed_bounds) and (not dynamic_bounds):
                            raise ValueError("fixed_bounds or dynamic_bounds have to be selected")

                        counter = 0
                        lower_bound = fixed_bounds[0] if bool(
                            fixed_bounds) else starting_temp + error_adjust - dynamic_bounds
                        upper_bound = fixed_bounds[1] if bool(
                            fixed_bounds) else starting_temp - error_adjust + dynamic_bounds

                        if (lower_bound >= upper_bound):
                            # print(f"Lower bound {lower_bound} >= upper bound {upper_bound}")
                            return 0

                        for idx, temp in enumerate(temps):
                            if (lower_bound <= temp <= upper_bound):
                                counter += 1
                            elif (is_sequential):
                                break

                            if (idx+1 == duration):
                                break

                        return counter

                    def extract_flexibility_row_foo( row,
                                            horizon: int,
                                            dynamic_threshold: float,
                                            duration: int,
                                            error_adjustment: float
                            ):
                        start_temperature = row['start_temperature']

                        # Error has to be precomputed before
                        # error_adjustment = 0
                        # if (error_adjustment_strategy == 'rmse/2'):
                        #     error_adjustment = row['rmse'] / 2 # can be none
                        # elif (error_adjustment_strategy == 'rmse'):
                        #     error_adjustment = row['rmse']

                        real_temps = row[0:horizon].to_numpy()
                        pred_temps = row[horizon: horizon*2].to_numpy()

                        real_flex = extract_flex(starting_temp=start_temperature,
                                                 temps=real_temps,
                                                 is_sequential=True,
                                                 dynamic_bounds=dynamic_threshold,
                                                 duration=duration,
                                                 error_adjust=0)
                        pred_flex = extract_flex(starting_temp=start_temperature,
                                                 temps=pred_temps,
                                                 is_sequential=True,
                                                 dynamic_bounds=dynamic_threshold,
                                                 duration=duration,
                                                 error_adjust=error_adjustment)
                        # return (pred_flex,real_flex, pred_flex-real_flex)
                        return f"{pred_flex}; {real_flex}; {pred_flex-real_flex}"

                    # ---------------------------------------------------------

                    test_rmse = rmse_error_for_adjustment
                    for err_strat, error_adjustment in [
                        ('none', 0),
                        ('rmse/2', test_rmse/2),
                        ('rmse', test_rmse),
                    ]:
                        for dyn_thres in [0.5, 1.0, 2.0, 4.0]:

                            # TODO: repeating code block (1) for mode ON
                            force_on_df_list = []
                            for (on_duration, on_filter) in [(3, force_on_filter_3), (4, force_on_filter_4)]:
                                on_df = force_on_df[on_filter]
                                on_df['duration'] = on_duration
                                if (len(on_df) == 0):
                                    # raise ValueError(f"There are no records found for OFF filter with duration {off_duration}")
                                    print(
                                        f"There are no records found for OFF filter with duration {on_duration}")
                                    # NOTE to self: this loop my look a little bit loopy (ha)
                                    # in reality, it operates on two nonintersecting parts of the dataframe with each iteration
                                    # result: if one filter len returns 0 -> we don't have records that satisfy that filter
                                    continue

                                on_df[f'ON__{err_strat}_{dyn_thres}'] = on_df.apply(
                                    lambda row: extract_flexibility_row_foo(row,
                                                                            horizon=horizon,
                                                                            duration=on_duration,
                                                                            dynamic_threshold=dyn_thres,
                                                                            error_adjustment=error_adjustment),
                                    axis=1
                                )
                                force_on_df_list.append(on_df)
                            force_on_df = pd.concat(force_on_df_list)

                            # TODO: repeating code block (1) for mode OFF
                            force_off_df_list = []
                            for (off_duration, off_filter) in [(3, force_off_filter_3), (4, force_off_filter_4)]:
                                off_df = force_off_df[off_filter]
                                off_df['duration'] = off_duration
                                if (len(off_df) == 0):
                                    # raise ValueError(f"There are no records found for OFF filter with duration {off_duration}")
                                    print(
                                        f"There are no records found for OFF filter with duration {off_duration}")
                                    # NOTE to self: this loop my look a little bit loopy (ha)
                                    # in reality, it operates on two nonintersecting parts of the dataframe with each iteration
                                    # result: if one filter len returns 0 -> we don't have records that satisfy that filter
                                    continue
                                off_df[f'OFF__{err_strat}_{dyn_thres}'] = off_df.apply(
                                    lambda row: extract_flexibility_row_foo(row,
                                                                            horizon=horizon,
                                                                            duration=off_duration,
                                                                            dynamic_threshold=dyn_thres,
                                                                            error_adjustment=error_adjustment),
                                    axis=1
                                )
                                force_off_df_list.append(off_df)

                            force_off_df = pd.concat(force_off_df_list)

                    force_off_df.to_csv(f"{flex_off_output_path}", index=True, header=True)
                    force_on_df.to_csv(f"{flex_on_output_path}", index=True, header=True)
                except Exception as e:
                    pd.DataFrame({'error': str(e)}, index=[0]).to_csv(os.path.join(full_folder_path, "flex_error.csv"))
                    print(f"Error occured during flex extraction: {e}")
                    raise(e)

            # for each of the trained models:
                # run flexibility extraction with and without error adjustment
                # calculate the underestimate | overestimate score
                # output that
                # output aggregate

        if (mode in ["full"]):
            print("\n--- Generating seasonal error aggregates ---\n")

            seasonal_output_dict_list = []
            for param_dict in param_list:
                full_folder_path = param_dict['full_folder_path']


                pass

        if (mode in ["full"]):
            print("\n--- Result aggregation ---\n")
            # Open output.csv
            # Open flexibility
            # Aggregate flexibility results
            # Append to output.csv
            pass

            aggregate_output_dict_list = []
            for param_dict in param_list:
                try:
                    full_folder_path = param_dict['full_folder_path']
                    flex_on_output_path = os.path.join(full_folder_path, "flex_on_output.csv")
                    flex_off_output_path = os.path.join(full_folder_path, "flex_off_output.csv")

                    flex_on_df = pd.read_csv(flex_on_output_path)
                    flex_off_df = pd.read_csv(flex_off_output_path)

                    output_df = pd.DataFrame.from_dict(param_dict)
                    # print(output_dict.columns)
                    columns_to_drop = ['seq_len', 'n_features', 'dropout', 'learning_rate',
                                       # 'power_threshold', # <<< KEEP THIS
                                       'input_type', 'site_ids', 'loss',
                                       'full_folder_path', 'checkpoint_path',
                                       'output_result_path',
                                       'add_time_features',
                                       ]

                    # This is done for compatibility reasons
                    if ('long_output_result_path' in output_df.columns):
                        columns_to_drop.append('long_output_result_path')

                    output_df = output_df.drop(columns_to_drop, axis=1)

                    # Aggregate the metrics
                    inference_result_df = pd.read_csv(param_dict['output_result_path'])
                    for metric in ['rmse', 'mae', 'maxe']:
                        output_df[metric] = inference_result_df[metric].mean()

                    # TODO: Aggregate flexibility

                    def generate_aggregate_flexibility_metrics(column_df):
                        column_start = DateUtils.now()

                        # Generate values (INEFFICIENT!!!!!!)
                        column_values = []
                        for _, row in column_df.iteritems():
                            values = list(map(lambda num_str: int(num_str.strip())
                                                                if num_str.strip() != 'nan' else np.nan,
                                                                         row.split(';')))
                            column_values.append(values)

                        # EVEN MORE INEFFICIENT!!!
                        pred_flex_sum = 0.
                        real_flex_sum = 0.
                        overestimation_sum = 0.
                        flex_count = 0. # column length
                        diff_sum = 0. # absolute sum of diffs
                        corr_pred_count = 0. # counter for diff == 0 (correct)
                        under_pred_count = 0. # counter for diff <= 1 (underpredicted)
                        flex_extracted_sum = 0.

                        for (pred_flex, real_flex, diff) in column_values:
                            flex_count += 1
                            real_flex_sum += real_flex
                            diff_sum += abs(diff)
                            if (diff <= 0):
                                pred_flex_sum += pred_flex
                                flex_extracted_sum += pred_flex
                                if (diff == 0):
                                    corr_pred_count += 1
                                else:
                                    under_pred_count += 1
                            elif (diff > 0):
                                overestimation_sum += diff
                                flex_extracted_sum += real_flex

                        overestimation_metric = overestimation_sum / max(flex_count, 1)
                        average_error_metric = diff_sum / max(flex_count, 1) #MAFE - Mean Absolute Flexibility Error
                        corr_under_ratio_metric = (corr_pred_count + under_pred_count) / max(flex_count, 1)
                        extracted_existing_flex_ratio = flex_extracted_sum / real_flex_sum # EPFR - Extracted Potential Flexibility Ratio

                        # print(f"Col takes: {DateUtils.now() - column_start}")
                        return (
                            real_flex_sum,
                            f"{average_error_metric:.3f}; {extracted_existing_flex_ratio:.3f}; {overestimation_metric:.3f}",
                            average_error_metric,
                            extracted_existing_flex_ratio,
                        )

                    # ---------------------------

                    for off_flex_column in list(filter(lambda col: col.startswith('OFF'), flex_off_df.columns)):
                        # value = reduce(lambda accumulator, item: item, off_column_df, initial=(0,0,0))
                        off_column_df = flex_off_df[off_flex_column]
                        output_df['REAL_off_flex'], output_df[off_flex_column], mafe, epfr = generate_aggregate_flexibility_metrics(off_column_df)
                        if (off_flex_column in ['OFF__none_0.5', 'OFF__none_1.0']):
                            output_df[f'MAFE__{off_flex_column}'] = f"{mafe:.3f}"
                            output_df[f'EPFR__{off_flex_column}'] = f"{epfr:.3f}"

                    for on_flex_column in list(filter(lambda col: col.startswith('ON'), flex_on_df.columns)):
                        on_column_df = flex_on_df[on_flex_column]
                        output_df['REAL_on_flex'], output_df[on_flex_column], mafe, epfr = generate_aggregate_flexibility_metrics(on_column_df)
                        if (on_flex_column in ['ON__none_0.5', 'ON__none_1.0']):
                            output_df[f'MAFE__{on_flex_column}'] = f"{mafe:.3f}"
                            output_df[f'EPFR__{on_flex_column}'] = f"{epfr:.3f}"

                    # ---- Seasonal stuff ----
                    full_output_result_path = param_dict['output_result_path']
                    full_output_df = pd.read_csv(full_output_result_path)

                    for month in range(0, 12 + 1):
                        output_for_month = full_output_df[full_output_df['month'] == month]
                        output_for_month_is_empty = (len(output_for_month) == 0)

                        output_df[f'rmse_{month}'] = output_for_month['rmse'].mean() \
                            if not output_for_month_is_empty \
                            else -1.0
                        output_df[f'maxe_{month}'] = output_for_month['maxe'].mean() \
                            if not output_for_month_is_empty \
                            else -1.0

                    # ---- AFTER EVERYTHING ----
                    aggregate_output_dict_list.append(output_df)

                except Exception as e:
                    pd.DataFrame({'error': str(e)}, index=[0]).to_csv(os.path.join(full_folder_path, "agg_error.csv"))
                    print(f"Error occured during result aggregation: {e}")

            aggregate_output_df = pd.concat(aggregate_output_dict_list)
            aggregate_output_df_path = os.path.join(output_dir, f'agg_output__{now_str}.csv')
            aggregate_output_df.to_csv(aggregate_output_df_path, index=False, header=True)

            try:
                for mode in ['OFF', 'ON']:
                    for bound in ['0.5', '1.0']:
                        for metric in ['MAFE', 'EPFR']:
                            plt.close()
                            rmse_metric = 'rmse'
                            y_metric = f'{metric}__{mode}__none_{bound}'
                            aggregate_output_df[y_metric] = pd.to_numeric(aggregate_output_df[y_metric],errors='coerce')
                            pl = aggregate_output_df[[rmse_metric, y_metric]].plot.scatter(x=rmse_metric, y=y_metric)
                            pl.set_xlabel('RMSE')
                            pl.set_ylabel(f'{metric}')
                            pl.set_title(f'{metric}, mode: {mode}, comfort bound: {bound}')
                            # plt.show() # savefig
                            file_path = f'{metric}_{mode}_{bound.replace(".","_")}.pdf'
                            absolute_file_path = os.path.join(output_dir, file_path)
                            plt.savefig(absolute_file_path)
                            plt.close()
            except Exception as e:
                pd.DataFrame({'error': str(e)}, index=[0]).to_csv(os.path.join(full_folder_path, "plot_error.csv"))
                print(f"Error occured during plotting: {e}")

            print("Aggregation done!")
except BaseException as e:
    print(f"Base exception occurred {e}!")
    raise(e)