#%%

# Imports command line argument parser
import argparse

# Re-loads all imports every time the cell is ran.
import torch.cuda
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping

import torchMetrics
from utils import *

# %load_ext autoreload
# %autoreload 2
pd.options.display.float_format = '{:,.5f}'.format

# Neural Networks
from pytorch_lightning import Trainer, seed_everything
from pytorch_lightning.loggers.csv_logs import CSVLogger

import experimentCommons

# Plotting
# %matplotlib inline

#%%
# --- Constants ---

# Column definitions
NEW_DATETIME_COL = 'DateTime'
DATETIME_COL = NEW_DATETIME_COL # for compat with old prepDataNYC NBook
AMBIENT_TEMP_COL = 'Ambient Outdoor Temperature'
ROOM_TEMP_COL = 'Room Air Temperature'
POWER_COL = "Total Unit Power"
SITE_ID_COL = "Site Identification"
DATE_COL = 'Date'
TIME_COL = 'Time'

DEFAULT_IDLE_POWER_THRESHOLD = 100.

DEFAULT_MINIMUM_OFF_INTERVAL_SIZE = 4
DEFAULT_MEMORY_RECORD_COUNT = 16

DEFAULT_INPUT_LENGTH = DEFAULT_MEMORY_RECORD_COUNT
DEFAULT_TARGET_LENGTH = DEFAULT_MINIMUM_OFF_INTERVAL_SIZE

DEFAULT_SITE_IDS = ["S43", "S53", "S47", "S52", "S46", "S40"]
# DEFAULT_SITE_IDS = ["S43"]

DEFAULT_SEED = 1
DEBUG = True # TODO: currently unused

formatted_datetime_now = DateUtils.now()

ap = argparse.ArgumentParser(
    description="This script runs training of neural networks for Low-dose CT image reconstruction tasks")
ap.add_argument("-m", "--model", required=True,
                help="model to train")
ap.add_argument("-d", "--datadir", required=True,
                help="path to dataset")
ap.add_argument("-si", "--site_ids", required=True,
                help="Site IDs for NYC dataset")
ap.add_argument("-o", "--outputdir", required=True,
                help="experiment output folder (for storing models, logs, etc.)")
ap.add_argument("-g", "--use_gpu", required=False,
                help="To enable GPU utilization")
ap.add_argument("-pt", "--power_threshold", required=True,
                help="Power threshold for OFF")
ap.add_argument("-xs", "--memory_length", required=True,
                help="Memory length for input")
ap.add_argument("-ys", "--horizon_length", required=True,
                help="Horizon length for output")
ap.add_argument("-b", "--batchsize", required=True,
                help="selected training batch size")
ap.add_argument("-l", "--lossfoo", required=True,
                help="loss function used for training")
ap.add_argument("-e", "--epochs", required=True,
                help="number of epochs (MAX if early stopping) for training")
# TODO: Data preprocessing type (n_features derived from that)
ap.add_argument("-n", "--name", required=False,
                help="fixed experiment name")
ap.add_argument("-nh", "--hidden_size", required=True,
                help="Hidden size for each layer")
ap.add_argument("-nl", "--n_layers", required=True,
                help="Number of hidden layers")
ap.add_argument("-dr", "--dropout_rate", required=True,
                help="Coefficient for dropout (xxx.xxx)")
ap.add_argument("-lr", "--learning_rate", required=True,
                help="Learning rate for optimizer")
# TODO: save models every n epochs
# ap.add_argument("-i", "--epochsaveinterval", required=False,
#                 help="number of epochs between saves")
ap.add_argument("-t", "--add_time_features", required=False,
                help="Add time features to data")
ap.add_argument("-dl", "--dataloader_type", required=True,
                help="Dataloader type full|interval")
ap.add_argument("-cl", "--is_colab", required=False,
                help="Is ran on google colab (reduces refresh rate)")
ap.add_argument("-s", "--seed", required=False,
                help=f"Seed for experiments (default is: {DEFAULT_SEED})")
ap.add_argument("-nw", "--no_weekend", required=False,
                help=f"Removes weekend values from train and val sets")

# ap.add_argument("-param_dict", "--optimizer", required=True,
#                 help="Select which optimizer to use (default one is 'adam')")
# ap.add_argument("-ctm", "--continue_train_model", required=False,
#                 help="Path to model which should be continued to trained")
# ap.add_argument("-cte", "--continue_train_model_initial_epoch", required=False,
#                 help="Initial epoch for the model which should be continued to be trained")
args = vars(ap.parse_args())

ARG_MODEL_NAME = args["model"]
ARG_DATA_DIR = args["datadir"]
ARG_SITE_IDS = args["site_ids"]
ARG_OUTPUT_DIR = args["outputdir"]
ARG_USE_GPU = args["use_gpu"]
ARG_POWER_THRESHOLD = args["power_threshold"]
ARG_MEMORY_LENGTH = args["memory_length"]
ARG_HORIZON_LENGTH = args["horizon_length"]
ARG_BATCH_SIZE = args["batchsize"]
ARG_LOSS_NAME = args["lossfoo"]
ARG_EPOCHS = args["epochs"]
ARG_EXPERIMENT_NAME = args["name"]
ARG_HIDDEN_SIZE = args["hidden_size"]
ARG_NUM_LAYERS = args["n_layers"]
ARG_DROPOUT_RATE = args["dropout_rate"]
ARG_LEARNING_RATE = args["learning_rate"]
# ARG_EPOCH_SAVE_INTERVAL = args["epochsaveinterval"]
# ARG_RESOLUTION = args["resolution"]
# ARG_TPU = args["use_tpu"]
# ARG_OPTIMIZER_NAME = args["optimizer"]
# ARG_CONTINUE_TRAIN_MODEL = args["continue_train_model"]
# ARG_CONTINUE_TRAIN_MODEL_EPOCH = args["continue_train_model_initial_epoch"]
ARG_ADD_TIME_FEATURES = args["add_time_features"]
ARG_DATALOADER_TYPE = args["dataloader_type"]
ARG_IS_COLAB = args["is_colab"]
ARG_SEED = args["seed"]
ARG_NO_WEEKEND = bool(args["no_weekend"])

#%%

'''
All parameters are aggregated in one place.
This is useful for reporting experiment params to experiment tracking software
'''

p = dict(
    seq_len = 1,
    batch_size = int(ARG_BATCH_SIZE),
    max_epochs = int(ARG_EPOCHS),
    hidden_size = int(ARG_HIDDEN_SIZE),
    num_layers = int(ARG_NUM_LAYERS),
    dropout = float(ARG_DROPOUT_RATE),
    learning_rate = float(ARG_LEARNING_RATE),
    site_ids = experimentCommons.get_site_ids_from_string(ARG_SITE_IDS),
    use_gpu = ARG_USE_GPU,
    experiment_name = ARG_EXPERIMENT_NAME if ARG_EXPERIMENT_NAME is not None else "unnamed_experiment",
    power_threshold = float(ARG_POWER_THRESHOLD),
    memory_length = int(ARG_MEMORY_LENGTH),
    horizon_length = int(ARG_HORIZON_LENGTH),
    data_dir = ARG_DATA_DIR,
    add_time_features = ARG_ADD_TIME_FEATURES,
    dataloader_type = ARG_DATALOADER_TYPE,
    progress_bar_refresh_rate = 2 if not ARG_IS_COLAB else 1000,
    is_colab = ARG_IS_COLAB,
    no_weekend = ARG_NO_WEEKEND,
    # TODO: make optimizer configurable as well
)

# TODO NOTE: this is tightly linked to 'collectOutputs.py' as well
p['n_features'] = 13 if (p["dataloader_type"] == "full") \
                     else 9 if (ARG_ADD_TIME_FEATURES) else 3  # TODO: tweak this when more features appear

#%%
# LOSS FUNCTION
# TODO: this is just "RMSE", maybe just drop it?

loss_map = experimentCommons.select_loss_map(p)
loss = loss_map.get(ARG_LOSS_NAME)
if (loss is None):
    str_list_of_available_losses = ", ".join(list(loss_map.keys()))
    raise ValueError(
        'Loss with name "' + ARG_LOSS_NAME + '" is not defined!\nList of available loss functions: ' + str_list_of_available_losses)
p["loss"] = loss
p["criterion"] = p["loss"] # For backwards compat

#%%
# MODEL

model_map = experimentCommons.select_model_map(p)
model = model_map.get(ARG_MODEL_NAME)
if (model is None):
    str_list_of_available_models = ", ".join(list(model_map.keys()))
    raise ValueError(
        'Model with name "' + ARG_MODEL_NAME + '" is not defined!\nList of available models: ' + str_list_of_available_models)

p["model"] = model
p['input_type'] = experimentCommons.get_appropriate_input_type(model_name=ARG_MODEL_NAME)

# ----------------------------------------------
# Setting up output directory

if ARG_OUTPUT_DIR is not None and Path(ARG_OUTPUT_DIR).is_dir():
    p["output_dir"] = ARG_OUTPUT_DIR
    FileUtils.create_dir(ARG_OUTPUT_DIR) # Just to make sure it exists
else:
    raise ValueError(f"{ARG_OUTPUT_DIR} is not a directory")

# ----------------------------------------------
# Setting up random seed

p["used_seed"] = int(ARG_SEED) if (ARG_SEED is not None) else DEFAULT_SEED
seed_everything(p["used_seed"])

# ----------------------------------------------

import pprint
print(f"Experiment setup: \n{ pprint.pformat(p, depth=1) }")

# ----------------------------------------------
# Running main experiment

before_training = DateUtils.now()

# Toggle GPU usage. Set to False if GPUs are unavailable (e.g. bad drivers)
GPU_AVAILABLE: bool = p['use_gpu'] and torch.cuda.is_available()

# Pathing for exports/outputs
LOGS_PATH = ARG_OUTPUT_DIR
EXPERIMENT_NAME = p['experiment_name']
EXPERIMENT_VERSION = DateUtils.formatted_datetime_now() + "__" + ARG_SITE_IDS.replace(',','-') + f"__la={ARG_NUM_LAYERS}_h={ARG_HIDDEN_SIZE}_e={ARG_EPOCHS}_m={ARG_MEMORY_LENGTH}"

EXPERIMENT_PATH = os.path.join(LOGS_PATH, EXPERIMENT_NAME, EXPERIMENT_VERSION)
csv_logger = CSVLogger(LOGS_PATH, name=EXPERIMENT_NAME, version=EXPERIMENT_VERSION),

### ----------------------- [ Checkpoint ] -------------------------

EXPERIMENT_CHECKPOINT_PATH = FileUtils.create_dir(os.path.join(EXPERIMENT_PATH, "checkpoints"))
EXPERIMENT_PERIODIC_CHECKPOINT_PATH = FileUtils.create_dir(os.path.join(EXPERIMENT_CHECKPOINT_PATH, "periodic"))
EXPERIMENT_BEST_CHECKPOINT_PATH = FileUtils.create_dir(os.path.join(EXPERIMENT_CHECKPOINT_PATH, "best"))

# saves a file like: my/path/sample-mnist-epoch=02-val_loss=0.32.ckpt
checkpoint_periodic_callback = ModelCheckpoint(
    monitor='val_loss',
    dirpath=EXPERIMENT_PERIODIC_CHECKPOINT_PATH,
    filename=f'{EXPERIMENT_NAME}_mod={ARG_MODEL_NAME}_mem={ARG_MEMORY_LENGTH}_hor={ARG_HORIZON_LENGTH}'+'-{epoch:03d}-{val_loss:.4f}',
    save_top_k=-1, # == 0: no models are saved, == -1: all models are saved, >= 2: callback called multiple times in epoch
    period=10,
    mode='min',
)# saves a file like: my/path/sample-mnist-epoch=02-val_loss=0.32.ckpt

checkpoint_best_callback = ModelCheckpoint(
    monitor='val_loss',
    dirpath=EXPERIMENT_BEST_CHECKPOINT_PATH,
    filename=f'{EXPERIMENT_NAME}_mod={ARG_MODEL_NAME}_mem={ARG_MEMORY_LENGTH}_hor={ARG_HORIZON_LENGTH}'+'-{epoch:03d}-{val_loss:.4f}',
    save_top_k=1, # == 0: no models are saved, == -1: all models are saved, >= 2: callback called multiple times in epoch
    mode='min',
)

early_stop_callback = EarlyStopping(
   monitor='val_loss',
   min_delta=0.02,
   patience=25,
   verbose=False,
   mode='min'
)

# -------------------------------------------------------------------

trainer = Trainer(
    max_epochs=p['max_epochs'],
    logger=csv_logger,
    gpus=1 if GPU_AVAILABLE else 0,
    log_every_n_steps=1,
    progress_bar_refresh_rate=p['progress_bar_refresh_rate'],
    # fast_dev_run=True,
    callbacks=[
        checkpoint_best_callback,
        checkpoint_periodic_callback,
    ],
    default_root_dir=EXPERIMENT_CHECKPOINT_PATH,
)

dm = experimentCommons.get_data_module(
    type=p['dataloader_type'],
    param_dict=p,
    no_weekend=p['no_weekend'], # <<< use only for training / testing
)
dm.setup('fit')

trainer.fit(model, dm)
test_results = trainer.test(model, datamodule=dm)

# -----------------------------------------------------------------

before_predictions = DateUtils.now()

# Plot report

#%%
import matplotlib.pyplot as plt

METRICS_PATH = os.path.join(EXPERIMENT_PATH, "metrics.csv")
PLOT_PATH = os.path.join(EXPERIMENT_PATH, "plot.png")

metrics = pd.read_csv(METRICS_PATH)
train_loss = metrics[['train_loss', 'step', 'epoch']][~np.isnan(metrics['train_loss'])]
val_loss = metrics[['val_loss', 'epoch']][~np.isnan(metrics['val_loss'])]
test_loss = metrics['test_loss'].iloc[-1]

last_epoch = int(metrics[['epoch']].max()) # NOTE: should be epoch_count - 1
# TODO: find out if we really need to aggregate with a mean in train_loss...
last_epoch_train_loss = train_loss[train_loss["epoch"] == last_epoch]["train_loss"].mean()
last_epoch_val_loss = float(val_loss[val_loss["epoch"] == last_epoch]["val_loss"])
last_epoch_test_loss = test_loss # Simply take the original test loss

# --------------- Plotting ------------------

fig, axes = plt.subplots(1, 2, figsize=(16, 5), dpi=600)
axes[0].set_title('Train loss per batch')
axes[0].plot(train_loss['step'], train_loss['train_loss'])
axes[0].set_yscale('log')
axes[1].set_title('Validation loss per epoch')
axes[1].plot(val_loss['epoch'], val_loss['val_loss'], color='orange', )
axes[1].set_yscale('log')
plt.savefig(fname=PLOT_PATH, block = True)

print(f'Loss {ARG_LOSS_NAME}:')
print(f"Train loss: {last_epoch_train_loss:.3f}")
print(f"Val loss:   {last_epoch_val_loss:.3f}")
print(f'Test loss:  {last_epoch_test_loss:.3f}')

# --------------- Export test set ----------------

EXPERIMENT_VISUALS_PATH = os.path.join(EXPERIMENT_PATH, "testVisuals")
FileUtils.create_dir(EXPERIMENT_VISUALS_PATH)

EXPERIMENT_OFF_VISUALS_PATH = ''
if (p['dataloader_type'] == 'full'):
    EXPERIMENT_OFF_VISUALS_PATH = os.path.join(EXPERIMENT_PATH, "testOffVisuals")
    FileUtils.create_dir(EXPERIMENT_OFF_VISUALS_PATH)

test_dm = experimentCommons.get_data_module(
    param_dict=p,
    type=p['dataloader_type'],
    input_type=p['input_type'],
    override_batch_size = 1,
    no_weekend=p['no_weekend'],
)
test_dm.setup('fit')

# That's how it works
trainer.test(model)

model.to('cpu')  # moved model to CPU

print("---------------------")
test_dl = test_dm.test_dataloader()
print(f"Length of dataloader: {len(test_dl)}")
test_loss = torchMetrics.RMSELoss()

# ----------------------------------------------

after_predictions = DateUtils.now()

formatted_datetime_experiment_end = DateUtils.now()
report_message = """
Model trained: {}
Loss function used: {}
Optimizer used: default
Trained epoch count: {}
Batch size: {}
Dataset used: {}
Site IDs: {}
Experiment started: {}
Experiment ended: {}
Validation and test loss calculation duration: {}
Experiment output directory: {}

---
Calculated train loss: {}
Calculated validation loss: {}
Calculated test loss: {}
---

Parameter object: {}
""".format(ARG_MODEL_NAME,
           ARG_LOSS_NAME,
           # ARG_OPTIMIZER_NAME,
           p["max_epochs"],
           ARG_BATCH_SIZE,
           # IMAGE_RESOLUTION,
           # IMAGE_RESOLUTION,
           ARG_DATA_DIR,
           p["site_ids"],
           formatted_datetime_now,
           formatted_datetime_experiment_end,
           str(after_predictions - before_predictions),
           EXPERIMENT_PATH,
           str(last_epoch_train_loss),
           str(last_epoch_val_loss),
           str(last_epoch_test_loss),
           pprint.pformat(p, depth=1)
           )

report_file_path = os.path.join(EXPERIMENT_PATH, "{}_report.txt".format(EXPERIMENT_NAME))
report_file = open(report_file_path, "w")
report_file.write(report_message)
report_file.close()

# -----------------------------------------------------------------
# Generating output object for the experiment

output_object = {}
output_object['date'] = before_training
output_object['experiment_name'] = ARG_EXPERIMENT_NAME
output_object['model'] = ARG_MODEL_NAME
output_object['layers'] = p['num_layers']
output_object['hidden_size'] = p['hidden_size']
output_object['memory_length'] = p['memory_length']
output_object['horizon_length'] = p['horizon_length']
output_object['epochs'] = p['max_epochs']
output_object['batch_size'] = p['batch_size']
output_object['loss_function'] = ARG_LOSS_NAME
output_object['training_time'] = before_predictions - before_training
output_object['dataloader_type'] = p['dataloader_type']
output_object['site_ids'] = p['site_ids']
output_object['time_features'] = p['add_time_features']
output_object['data_dir'] = p['data_dir']
output_object['output_dir'] = p['output_dir']
output_object['train_loss'] = f"{last_epoch_train_loss:.3f}"
output_object['val_loss'] = f"{last_epoch_val_loss:.3f}"
output_object['test_loss'] = f"{last_epoch_test_loss:.3f}"

pd.DataFrame(output_object).to_csv(os.path.join(EXPERIMENT_PATH, "output_object.csv"), index=False, header=True)