# Snippets of code



############# Pandas section ################

import pandas as pd

# Gives a nice table with percentiles and other useful stats
def stats_for_dataframe(df: pd.DataFrame):
    return df.describe()

def get_index_numeric_location_by_proper_index(df: pd.DataFrame, index_value):
    return df.index.get_loc(index_value)

def generate_example_df():
    return pd.DataFrame({
        'A': [num for num in range(1, 10+1)],
        'B': [num for num in range(10, 20)]
    })